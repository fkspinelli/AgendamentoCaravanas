$.datepicker.regional['pt-BR'] = {
  closeText: 'Fechar',
  prevText: '&#x3c',
  nextText: '&#x3e;',
  currentText: 'Hoje',
  monthNames: ['Janeiro de','Fevereiro de','Mar&ccedil;o de','Abril de','Maio de','Junho de',
  'Julho de','Agosto de','Setembro de','Outubro de','Novembro de','Dezembro de'],
  monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
  'Jul','Ago','Set','Out','Nov','Dez'],
  dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
  dayNamesShort: ['D','S','T','Q','Q','S','S'],
  dayNamesMin: ['D','S','T','Q','Q','S','S'],
  weekHeader: 'Sm',
  dateFormat: 'dd/mm/yy',
  firstDay: 0,
  isRTL: false,
  showMonthAfterYear: false,
  yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

$(document).ready(function(){

 $( "#from" ).datepicker({
      
     dateFormat: "dd/mm/yy",
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      
      dateFormat: "dd/mm/yy",
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
    $('select#state_id').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('#city_id').html(data.response);
        });
    });
            //get zipcode 
     $('input[name="cep"]').blur(function(){
      var cep_code = $(this).val();

      if( cep_code.length < 9 ){
        return;
      } else{
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
           
            $("select#state_id option:contains('"+result.state+"')").prop('selected',true);
            var state = $("select#state_id option:contains('"+result.state+"')").val();
            
            $.ajax({
                url:'/city/search',
                type:'post',
                data:{state:state}
            }).done(function(data){
                
                $('#city_id').html(data.response);
                $('#city_id option:contains('+result.city+')').prop('selected',true);

            });
            
            $('input[name="district"]').val( result.district );
            $('input[name="address"]').val( result.address );
             

            $('input[name="reference"]').focus();
            
         });
      }
      
    });
    
    $('input[name="selec_all"]').click(function(){
      if($(this).prop('checked') == true){
        $('.agenda_ids').prop('checked',true);
      }else{
        $('.agenda_ids').prop('checked',false);
      }
    });

    $('#btn_action_agenda').click(function(){
      var valor = $('select[name="actionAgenda"]').val();
      
      $('#frmAgendaAction input[name="action"]').val(valor);
      
      if(valor == '2'){
        $('#modal_justify').modal('show');
        return;
      }
      $('#frmAgendaAction').submit();
    });

    $('.btn_loader').click(function(){
      $('#modalLoading').modal();
    });

    $('#btn_action_justify').click(function(){
      var valor = $('select[name="actionAgenda"]').val();
      var justify = $('#justify_text').val();
      
      $('#frmAgendaAction input[name="justify"]').val(justify);
      $('#frmAgendaAction input[name="action"]').val(valor);
      $('#frmAgendaAction').submit();
    });

    $('input[name="cep"]').mask('99999-999');
    $('input[name="tel"]').mask('(99) 9999-9999?9');
    $('input[name="cpf"]').mask('999.999.999-99');
     $('input[name="date_marked"]').mask('99/99/9999');

     $('input[name="birthday"]').mask('99/99/9999');
});