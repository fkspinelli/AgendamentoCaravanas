$.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c',
    nextText: '&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro de','Fevereiro de','Mar&ccedil;o de','Abril de','Maio de','Junho de',
    'Julho de','Agosto de','Setembro de','Outubro de','Novembro de','Dezembro de'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
    dayNamesShort: ['D','S','T','Q','Q','S','S'],
    dayNamesMin: ['D','S','T','Q','Q','S','S'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['pt-BR']);
$(document).ready(function(){
	$('.aceitar-termo').click(function(){
		$('.termos-condicoes ').show();
		$('.bg-lightbox').show().animate({'opacity':'0.8'},'slow');
		$('.lightbox').show().animate({'opacity':'1', 'zoom': '100%'},'slow');
		$('html, body').animate({ scrollTop: 0 }, 'slow');
	});

	$('.close-lightbox ').click(function(){
		$('html, body').animate({ scrollTop: $(window).height() }, 'slow');
		$('.lightbox').animate({'zoom': '30%', 'opacity':'0'},'slow',function(){
			$(this).hide();
		});
		$('.bg-lightbox').animate({'opacity':'0'},'slow',function(){
			$(this).hide();
			$('.termos-condicoes ').hide();
		});
	});

	if($('.alert').length > 0){
		var pos = ($('.alert').offset().top - 20);
		$('html, body').animate({ scrollTop: pos}, 'slow');
	}

	$('select#state_id').change(function(){
        var state = $(this).val();
        
        $.ajax({
            url:'/city/search',
            type:'post',
            data:{state:state}
        }).done(function(data){
            
            $('#city_id').html(data.response);
        });
    });

    $('input[name="tel"]').mask('(99) 9999-9999?9');
    $('input[name="cel"]').mask('(99) 9999-9999?9');
    $('input[name="cpf"]').mask('999.999.999-99');
    $('input[name="birthday"]').mask('99/99/9999');
    $('input[name="cep"]').mask('99999-999');

	//Datepicker
	$(function() {
		$( "#datepicker" ).datepicker({
			minDate: "+2D", 
			showOtherMonths: true,
			altFormat: "yy-mm-dd",
			beforeShowDay: function(date) {
				var enableDates = ['10/12/2017','17/12/2017']

		        var day = date.getDay();

		        var format = date.getDate()+"/"+(date.getMonth()+1)+"/"+date.getFullYear()

				var dateShow = enableDates.find(function checkAdult(d) {
				    return d == format;
				})

				if (dateShow != undefined) {
					return [true];
				}

				if (day != 0) {
					return [true];
				}
				return [false];
		    },
			onSelect: function (dateText, inst){
				var date = $(this).datepicker('getDate');
				var dayName = $.datepicker.formatDate('DD', date);
    
				$('.day_choosen .data').text(dateText);
				$('input.data').val(dateText);
				$('.day_choosen .dia').text(dayName);
				$('#calendar_detail').fadeIn();

	        },
		});
	});

	$('#frmAgendamento').validate({
		rules:{
			institute_id:{required:true},
			period_id:{required:true},
			num_group:{required:true, min:10},
		},
		messages:{
			
			num_group:{min:"Deve ser no mínimo 10."},
		}
	});

	// Action leader
	$('.cancelar-lider').click(function(){
		if($(this).data('action') == 'sim'){
			var confirm = window.confirm("Você deseja ser uma líder de Caravanas BN?");
		}else{
			var confirm = window.confirm("Você deseja deixar de ser líder mesmo?");
		}
		

		if(confirm == true){
			window.location.href = '/group/edit';
		}
	});

	$('select[name="institute_id"]').change(function(){
		window.location.href = '/agendamento/'+$(this).val();
	});


	//upload de imagem com preview
	$("#fileUpload").on('change', function () {

	     //Get count of selected files
	     var countFiles = $(this)[0].files.length;

	     var imgPath = $(this)[0].value;
	     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	     var image_holder = $("#image-holder");
	     image_holder.empty();

	     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
	         if (typeof (FileReader) != "undefined") {

	             //loop for each file selected for uploaded.
	             for (var i = 0; i < countFiles; i++) {

	                 var reader = new FileReader();
	                 reader.onload = function (e) {
	                     $("<img />", {
	                         "src": e.target.result,
	                             "class": "thumb-image"
	                     }).appendTo(image_holder);
	                 }

	                 image_holder.show();
	                 reader.readAsDataURL($(this)[0].files[i]);
	             }

	         } else {
	             alert("This browser does not support FileReader.");
	         }
	     } else {
	         alert("Pls select only images");
	     }
	 });

          //get zipcode 
     $('input[name="cep"]').blur(function(){
      var cep_code = $(this).val();

      if( cep_code.length < 9 ){
        return;
      } else{
        $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
           
            $("select#state_id option:contains('"+result.state+"')").prop('selected',true);
            var state = $("select#state_id option:contains('"+result.state+"')").val();
            
            $.ajax({
                url:'/city/search',
                type:'post',
                data:{state:state}
            }).done(function(data){
                
                $('#city_id').html(data.response);
                $('#city_id option:contains('+result.city+')').prop('selected',true);

            });
            
            // $('input[name="district"]').val( result.district );
            $('input[name="address"]').val( result.address );
             

            $('input[name="tel"]').focus();
            
         });
      }
      
    });

	$('#cab input').click(function(){
		if($(this).val() == 1){
			$('input[name="cab_num"]').slideDown();
		}else{
			$('input[name="cab_num"]').slideUp().val('');
		}
	});

});