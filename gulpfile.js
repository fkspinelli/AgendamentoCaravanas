var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.less('app.less'); 

    mix.styles(['modal.css', 'pq_select.css', 'pricing.css','jquery.rating.css', 'jquery-ui.min.css','jquery-ui.theme.css', 'jquery.bxslider.css'],'public/css/vendor.css');

    mix.scripts(['jquery.min.js', 'jquery-ui.min.js', 'jquery.modal.js'], 'public/js/main.js', 'resources/assets/js')
    .scripts(['bootstrap.min.js', 'jquery.validate.js', 'jquery.mask.js','jquery-select.js', 'jquery.bxslider.min.js', 'jquery.rating.js'], 'public/js/vendor.js', 'resources/assets/js')
    .scripts(['scripts.js'], 'public/js/general.js', 'resources/assets/js')
    .scripts(['scripts.admin.js'], 'public/js/admin.js', 'resources/assets/js');

    mix.version(['css/vendor.css']);
});
