<?php 

return array(

	'docError' => ':element inválido',
	'notfound' => 'Usuário não encontrado',
	'activated' => 'Usuário ativado com sucesso, agora já pode se logar e participar de todos nossos eventos e promoções',
	'notactivated' => 'Erro ao ativar usuário',

	'register' => array(
		'success' => array(
			'colaborator' => '<h4>Parabéns! Você agora é uma Líder de Caravana!</h4><p>Você receberá um e-mail com as regras e benefícios de se tornar uma Líder.</p>',
			'guest' => '<h4>Parabéns! Você agora é uma Participante de Caravana!</h4><p>Você receberá um e-mail de boas vindas e a partir de agora poderá encontrar as Caravanas mais próximas para sua cidade. Seja bem-vinda!</p>'
		),
		'error' => 'Erro ao cadastrar usuário, tente novamente mais tarde',
		'exists' => 'Usuário já existente!',
		'required' => array(
			'login' => 'Email obrigatório',
			'pass' => 'Senha obrigatória'
		)
	),
	'edit' => array(
		'group' => array(
			'leader' => 'Você deixou de ser Líder',
			'guest' => 'Parabéns, você agora é um(a) Líder',
		),
		'success' => 'Usuário editado com sucesso',
		'error' => 'Erro ao editar',
	)
);