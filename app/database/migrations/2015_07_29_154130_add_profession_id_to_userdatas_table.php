<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddProfessionIdToUserdatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('userdatas', function(Blueprint $table)
		{
			$table->integer('profession_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('userdatas', function(Blueprint $table)
		{
			$table->dropColumn('profession_id');
		});
	}

}
