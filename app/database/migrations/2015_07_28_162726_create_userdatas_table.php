<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserdatasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userdatas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username');
			$table->integer('city_id');
			$table->string('cpf');
			$table->integer('cab');
			$table->string('tel');
			$table->string('cel');
			$table->string('fb_profile');
			$table->string('src');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userdatas');
	}

}
