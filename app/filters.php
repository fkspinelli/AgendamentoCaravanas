<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if(!Sentry::check()){
		return Redirect::route('user.login');
	}
});
Route::filter('auth.admin', function()
{
		if(!Sentry::getUser()->hasAccess('admin')){
			Sentry::logout();
			return Redirect::route('admin.index')->withError('Você não possui esta permissão de acesso.');
		}
});
Route::filter('auth.manager', function()
{		
		$u = Sentry::getUser();


		if(!$u->hasAccess('manager')){
			Sentry::logout();
			return Redirect::back()->withError('Você não possui esta permissão de acesso.');
		}
		
});
Route::filter('institute.manager', function($route)
{
	$u = Sentry::getUser();
	$user = User::find($u->id);

	$slug = $route->getParameter('slug');

	if($user->manager->institute->slug != $slug){
		Sentry::logout();
		return Redirect::route('manager.index', $slug)->withError('Você não é Administrador deste instituto, tente '.url().'/instituto/'.$user->manager->institute->slug.'.');
	}

});
Route::filter('already.log', function(){
	if(Sentry::check()){
		if(Sentry::getUser()->hasAccess('colaborator')){
			return Redirect::route('agendamento.create');
		}
		return Redirect::route('institutes.leaders');
	}

});
Route::filter('already.log.admin', function(){
	if(Sentry::check()){
		if(Sentry::getUser()->hasAccess('admin')){
			return Redirect::route('admin.institutos.index');
		}
		//return Redirect::route('home');
	}

});

Route::filter('colaborator.access', function(){
	if(!Sentry::getUser()->hasAccess('colaborator')){
		return Redirect::route('institutes.leaders');
	}
});

Route::filter('guest.access', function(){
	if(!Sentry::getUser()->hasAccess('guest')){
		return Redirect::route('agendamento.create');
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
