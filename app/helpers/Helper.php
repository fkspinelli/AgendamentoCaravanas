<?php 
class Helper{

    static function ConverterBR($data=null, $justdate=false) {
        if($data==null){
            return null;
        }
        if($justdate==false){
            list($dat,$hor) = explode(" ", $data);
            list($a, $m, $d) = explode("-", $dat);
        }else{
            list($a, $m, $d) = explode("-", $data);
        }
        
        
        return "$d/$m/$a";
    }
    static function Hora($data){
        list($dat,$hor) = explode(" ", $data);

        return $hor;
    }

    static function Monetize($n){
        return number_format($n, 2, ',', '.');
    }

    static function ConverterUS($data) {
        list($d, $m, $a) = explode("/", $data);
        return "$a-$m-$d";
    }

    static function AdicionarDias($data, $dias) {
        list($a, $m, $d) = explode("-", $data);
        return date("Y-m-d", mktime(0, 0, 0, $m, ($d + $dias), $a));
    }

    static function AdicionarMeses($data, $meses) {
        list($a, $m, $d) = explode("-", $data);
        return date("Y-m-d", mktime(0, 0, 0, ($m + $meses), $d, $a));
    }

    static function SubtrairMeses($data, $meses) {
        list($a, $m, $d) = explode("-", $data);
        return date("Y-m-d", mktime(0, 0, 0, ($m - $meses), $d, $a));
    }

    static function AdicionarMesesFixo($data, $meses) {
        list($a, $m, $d) = explode("-", $data);

        $ano = $a + (floor($meses / 12));
        $mes = $m + ($meses - (floor($meses / 12) * 12));
        if ($mes > 12) {
            $ano++;
            $mes = ($mes - 12);
        }

        $ultimo_dia_atual = date("t", mktime(0, 0, 0, $m, $d, $a));

        if ($ultimo_dia_atual == $d) {
            $dia = date("t", mktime(0, 0, 0, $mes, 1, $ano));
        } else {
            if (checkdate($mes, $d, $ano)) {
                $dia = $d;
            } else {
                $dia = date("t", mktime(0, 0, 0, $mes, 1, $ano));
            }
        }
        return "$ano-$mes-$dia";
    }

    static function AdicionarAnos($data, $anos) {
        list($a, $m, $d) = explode("-", $data);
        return date("Y-m-d", mktime(0, 0, 0, $m, $d, ($a + $anos)));
    }

    static function DiasEntreDatas($data1, $data2) {
        list($a1, $m1, $d1) = explode('-', $data1);
        list($a2, $m2, $d2) = explode('-', $data2);

        if ($data1 > $data2) {
            $dias = floor(((mktime(0, 0, 0, $m1, $d1, $a1) - mktime(0, 0, 0, $m2, $d2, $a2)) / 86400));
        } else {
            $dias = floor(((mktime(0, 0, 0, $m2, $d2, $a2) - mktime(0, 0, 0, $m1, $d1, $a1)) / 86400));
        }
        return $dias;
    }

    static function MesesEntreDatas($data1, $data2) {
        list($a1, $m1, $d1) = explode('-', $data1);
        list($a2, $m2, $d2) = explode('-', $data2);

        if ($data1 > $data2) {
            $meses = floor(((mktime(0, 0, 0, $m1, $d1, $a1) - mktime(0, 0, 0, $m2, $d2, $a2)) / 2592000));
        } else {
            $meses = floor(((mktime(0, 0, 0, $m2, $d2, $a2) - mktime(0, 0, 0, $m1, $d1, $a1)) / 2592000));
        }
        return $meses;
    }

    static function AnosEntreDatas($data1, $data2) {
        list($a1, $m1, $d1) = explode('-', $data1);
        list($a2, $m2, $d2) = explode('-', $data2);

        if ($data1 > $data2) {
            $anos = floor(((mktime(0, 0, 0, $m1, $d1, $a1) - mktime(0, 0, 0, $m2, $d2, $a2)) / 31536000));
        } else {
            $anos = floor(((mktime(0, 0, 0, $m2, $d2, $a2) - mktime(0, 0, 0, $m1, $d1, $a1)) / 31536000));
        }
        return $anos;
    }

    static function UltimoDiaMes($data,$formato_data=false) {
        list($a, $m, $d) = explode('-', $data);
        $ultimo = date("t", mktime(0, 0, 0, $m, $d, $a));
        if($formato_data){
          $retorno = date('Y-m-d',mktime(0,0,0,($m + 1),($d - 1),$a));
        }else{
          $retorno = $ultimo;
        }
        return $retorno;
    }

    static function ValidaDataUS($data) {
        list($a, $m, $d) = explode('-', $data);
        return checkdate($m, $d, $a);
    }

    static function ValidaDataBR($data) {
        list($d, $m, $a) = explode('/', $data);
        return checkdate($m, $d, $a);
    }

    static function Mes($data, $time=false) {
         if($time == true){
            list($dat, $h) = explode(' ',$data);
            list($a, $m, $d) = explode('-', $dat);
        }else{
        list($a, $m, $d) = explode('-', $data);
        }
        return $m;
    }

    static function Ano($data, $time=false) {
        if($time == true){
            list($dat, $h) = explode(' ',$data);
            list($a, $m, $d) = explode('-', $dat);
        }else{
        list($a, $m, $d) = explode('-', $data);
        }
        return $a;
    }

    static function Dia($data,$time=false) {
       if($time == true){
        list($dat, $h) = explode(' ',$data);
        list($a, $m, $d) = explode('-', $dat);
       }else{
        list($a, $m, $d) = explode('-', $data);
       }
        
        
        return $d;
    }

    static function LimparFormatacao($data) {
        return str_replace(array("/", "-"), "", $data);
    }

    static function DiaSemana($data) {// 0=DOMINGO  /  6=SABADO
        list($a, $m, $d) = explode('-', $data);
        switch(date("w", mktime(0, 0, 0, $m, $d, $a))){
            case '1':
                $res = 'Segunda-feira';
            break;
            case '2':
                $res = 'Terça-feira';
            break;
            case '3':
                $res = 'Quarta-feira';
            break;
            case '4':
                $res = 'Quinta-feira';
            break;
            case '5':
                $res = 'Sexta-feira';
            break;
            case '6':
                $res = 'Sábado';
            break;
            default:
                $res = 'Domingo';
            break;
        }

        return $res;
    }

    static function Extenso($c, $data = false, $ini_mes=null) {
        $meses = array(1 => "janeiro", 2 => "fevereiro", 3 => "março", 4 => "abril", 5 => "maio", 6 => "junho", 7 => "julho", 8 => "agosto", 9 => "setembro", 10 => "outubro", 11 => "novembro", 12 => "dezembro");
        if (!$data) {
            $data = date("Y-m-d");
        }
        $d = date_parse($data);
        if($ini_mes==true){
           $m = $meses[$d['month']];
           $m = substr($m, 0,3);

           return $m;
        }else{
             $m = $meses[$d['month']];
            return ($c ? $c . ", " : "") . "$d[day] de $m de $d[year]";
        }
       
    }

    static function mesExtenso($data){
        $meses = array(1 => "janeiro", 2 => "fevereiro", 3 => "março", 4 => "abril", 5 => "maio", 6 => "junho", 7 => "julho", 8 => "agosto", 9 => "setembro", 10 => "outubro", 11 => "novembro", 12 => "dezembro");
        if (!$data) {
            $data = date("Y-m-d");
        }
        $d = date_parse($data);
        $m = $meses[$d['month']];

        return $m;

    }

    static function PeriodoEntreDatas($Data1, $Data2) {

        $d1 = self::ConverterUS($Data1);
        $d2 = self::ConverterUS($Data2);

        $dias = self::DiasEntreDatas($d1, $d2);
        $anos = floor($dias / 365);
        $dias -= $anos * 365;
        $meses = floor($dias / 30);
        $dias -= $meses * 30;
        $dados = array('dias' => $dias, 'meses' => $meses, 'anos' => $anos, 'periodo' => "$anos anos, $meses meses, $dias dias.");
        return $dados;
    }

    static function MesesPor30($Dias, $cheio = true) {
        $meses = $Dias / 30;
        if ($cheio) {
            $resto = $Dias % 30;
            if ($resto) {
                $meses++;
            }
        }
        return $meses;
    }

    static function IdentificarData($data, $padrao = false) {
        $dt = str_replace(array("-", "/", " "), "", $data);
        $tam = strlen($dt);
        if ($tam == 8) {
            $v = substr($dt, 4, 2);
            if ($v > 12) { /* 28041990 */
                $dia = substr($dt, 0, 2);
                $mes = substr($dt, 2, 2);
                $ano = substr($dt, 4, 4);
            } else { /* 19900428 */
                $dia = substr($dt, 6, 2);
                $mes = substr($dt, 4, 2);
                $ano = substr($dt, 0, 4);
            }
            if ($ano < 1900 || $mes > 12) {
                $r = NULL;
            } else {
                if (!checkdate($mes, $dia, $ano)) {
                    $r = NULL;
                } else {
                    $r = "$ano-$mes-$dia";
                }
            }
        } else {
            $r == NULL;
        }
        if ($r == NULL && $padrao) {
            $r = $padrao;
        }
        return $r;
    }

    static function respostaPesquisa($value){
        switch ($value) {
            case '1':
                $res = "Suficiente";
                break;
            case '2':
                $res = "Bom";
            break;
            case '3':
                $res = "Excelente";
            break;
            default:
                $res = null;
            break;
        }
        return $res;
    }
}