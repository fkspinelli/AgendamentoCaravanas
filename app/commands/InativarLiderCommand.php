<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class InativarLiderCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'lideres:inativar';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $logFile = 'atualizar-lideres-inativos.log';

        \Illuminate\Support\Facades\Log::useDailyFiles(storage_path().'/logs/'.$logFile);

	    $lideresSemAgenda = \Illuminate\Support\Facades\DB::connection()->select("
        SELECT PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM NOW()), EXTRACT(YEAR_MONTH FROM a.date_marked)) months,
               a.date_marked, user_id
        FROM
          (
          SELECT 
            max(agendas.date_marked) AS date_marked, agendas.user_id, users.lead_inactive
           FROM agendas 
           inner join users on users.id = agendas.user_id
           where users.lead_inactive = 0
           GROUP BY user_id
          ) AS a
        WHERE PERIOD_DIFF(EXTRACT(YEAR_MONTH FROM NOW()), EXTRACT(YEAR_MONTH FROM a.date_marked)) > 6
");

	    $total = 0;
	    foreach ($lideresSemAgenda as $lider)
        {
            $user = null;
            $user = User::find($lider->user_id);

            if ($user) {
                $user->lead_inactive = 1;
                $user->save();
                $total++;
                \Illuminate\Support\Facades\Log::info("{$lider->user_id} {$lider->months} {$lider->date_marked}");
            } else {
                \Illuminate\Support\Facades\Log::error("{$lider->user_id} {$lider->months} {$lider->date_marked}");
            }



        }

        echo $total;


	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [];
	}

}
