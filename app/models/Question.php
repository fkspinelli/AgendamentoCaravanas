<?php

class Question extends \Eloquent {
	protected $fillable = [];

	public function state(){
		return $this->belongsTo('State');
	}

	public function userdatas(){
		return $this->hasMany('Userdata');
	}

	public function institutes(){
		return $this->hasMany('Institute');
	}
}