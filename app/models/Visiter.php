<?php

class Visiter extends \Eloquent {
	protected $fillable = [];

	public static $rules = [
	'q1'=>'required',
	'q2'=>'required',
	'q3'=>'required',
	'q4'=>'required',
	'q5'=>'required',
	'q6'=>'required',
	'q7'=>'required',
	'q8'=>'required',
	'q9'=>'required',
	'q10'=>'required'
	];

	public function agenda()
	{
		return $this->belongsTo('Agenda');
	}

	public function institute()
	{
		return $this->belongsTo('Institute');
	}

	public function unserial()
	{
		//print_r(unserialize(base64_decode($this->result)));
		return unserialize(base64_decode($this->result));
	}
}