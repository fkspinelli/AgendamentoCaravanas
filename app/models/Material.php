<?php

class Material extends \Eloquent {
	protected $fillable = ['name','description','src'];

	public static $rules = [
		'name' => 'required',
		'description' => 'required',
		'src' => 'required'
	];

	public function user(){
		return $this->belongsTo('User');
	}

}