<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function userdata(){

		return $this->hasOne('Userdata');
	}

	public function profession(){
		return $this->hasOne('Profession');
	}

	public function manager(){
		return $this->hasOne('Manager');
	}

	public function institutes(){
		return $this->belongsToMany('Institute');
	}

	public function agendas(){
		return $this->hasMany('Agenda')->orderBy('date_marked','DESC');
	}

	public function agendas_posvisita_pendente($institute)
	{
		$agendas = Agenda::where(function($q){
			$q->whereIn('status',['0','1'])->where('date_marked','>',date('Y-m-d'));
		})
		->orWhere(function($q){
			$q->whereStatus('3')->has('visiter','=','0');
		})
		//->whereUser_id($this->id)
		->whereInstitute_id($institute)->get();
		//print_r(DB::getQueryLog());
		return $agendas;

		//->whereOr(function($q){$q->where('date_marked','>',date('Y-m-d'))})
			
		
	}

	public function getImage($url=false)
	{
		$src = 'img/avatar.png';
		if($this->userdata->src != ''){
			$src = 'uploads/'.$this->userdata->src;
		}

		return $url == true ? $src : '<img src="'.$src.'" alt="'.$this->name.'">';
	
	}
	public function scopeGetLeadersNearest($query, $state_id){
		return $query->join('userdatas','users.id', '=', 'userdatas.user_id')
				->join('cities', 'userdatas.city_id', '=', 'cities.id')
				->join('states', 'cities.state_id', '=', 'states.id')
				->where('states.id', $state_id)
				->select('users.*');
	}

	public function scopeGetAgendaInstitute($query, $slug, $realizado=false)
	{
		$query->join('agendas', 'users.id','=','agendas.user_id')
		->join('institutes', 'agendas.institute_id','=','institutes.id');
		if($realizado == true){
			$query->join('visiters', 'agendas.id', '=', 'visiters.agenda_id');
		}
		$query->where('institutes.slug',$slug)
		->orderBy('agendas.id', 'DESC')
		->select('users.*');
	}


	public static function boot(){
		parent::boot();

		static::deleted(function($u)
		{
			$u->userdata()->delete();
			DB::table('users_groups')->whereUser_id($u->id)->delete();
			
		});
	}

}
