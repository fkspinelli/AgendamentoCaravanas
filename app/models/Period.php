<?php

class Period extends \Eloquent {
	protected $fillable = ['name'];

	public $timestamps = false;
	
	public function agendas(){
		return $this->hasMany('Agenda');
	}


}