<?php

class Profession extends \Eloquent {
	protected $fillable = [];

	public function user(){
		return $this->belongsTo('User');
	}

	public function userdatas()
	{
		return $this->hasMany('Userdata');
	}
}