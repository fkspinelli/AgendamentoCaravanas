<?php

class Manager extends \Eloquent {
	protected $fillable = [];

	public function institute(){
		return $this->belongsTo('Institute');
	}

	public function user(){
		return $this->belongsTo('User');
	}
}