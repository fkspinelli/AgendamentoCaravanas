<?php

class Agenda extends \Eloquent {
	protected $fillable = ['num_group','period_id', 'institute_id', 'status'];

	public static $rules = ['num_group'=>'required|numeric|min:14','period_id'=>'required','institute_id'=>'required', 'date_marked'=>'required'];

	public function user(){
		return $this->belongsTo('User');
	}

	public function institute(){
		return $this->belongsTo('Institute');
	}

	public function period(){
		return $this->belongsTo('Period');
	}

	public function visiter()
	{
		return $this->hasOne('Visiter');
	}

	public function scopeOnlyConfirmed($query){
		return $query->whereStatus('1');
	}

	public function scopeGetByStatus($query, $status)
	{
		return $query->whereStatus($status);
	}

	public function scopeNotRealized($query)
	{
		return $query->whereStatus('2')->orWhere('status','0')->has('visiter','<','1');
	}
	public function statusText(){
		switch ($this->status) {
			case '1':
				$res = '<span style="display:inherit;" class="alert alert-info">Confirmado</span>';
			break;
			case '2':
				$res = '<span style="display:inherit;" class="alert alert-danger">Cancelado</span>';
			break;
			case '3':
				$res = '<span style="display:inherit;" class="alert alert-success">Realizado</span>';
			break;
			
			default:
				$res = '<span style="display:inherit;" class="alert alert-warning">Aguardando confirmação</span>';
				break;
		}

		return $res;
	}

}