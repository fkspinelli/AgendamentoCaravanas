<?php

class Institute extends \Eloquent {
	protected $fillable = ['name', 'tel', 'cep','address','district','business_hour','reference','city_id','state_id','active'];

	public static $rules = [
		'name' => 'required|unique:institutes',
		'tel' =>'required',
		'cep' =>'required',
		'address' =>'required',
		'district' =>'required',
		'business_hour' =>'required',
		'state_id' =>'required',
		'city_id' =>'required',
	];

	public function city(){
		return $this->belongsTo('City');
	}

	public function state(){
		return $this->belongsTo('State');
	}

	public function users(){
		return $this->belongsToMany('User');
	}

	public function managers(){
		return $this->hasMany('Manager');
	}

	public function agendas(){
		return $this->hasMany('Agenda');
	}

	public function visiters()
	{
		return $this->hasManyThrough('Visiter','Agenda');
	}

	public function getAgendasCount($status='0', $from="", $to="")
	{
		$agenda = Agenda::whereStatus($status)->whereInstitute_id($this->id);
		//$this->setDateRange($from, $to, $agenda);
		if($from != '' and $to != ''){
			$agenda->where('created_at', '>=', $from)
					->where('created_at','<=', $to);

		}
		return $agenda->count();
	}

	public function getVisitorsCount()
	{
		return Agenda::whereStatus('3')->whereInstitute_id($this->id)->has('visiter')->count();
	}
	public function getWithoutVisitorsCount()
	{
		return Agenda::whereStatus('3')->whereInstitute_id($this->id)->has('visiter','<','1')->count();
	}
	public function getNotRealized($from="", $to="")
	{
		$agenda = Agenda::where(function($q1){
			$q1->where('status','2')->orWhere('status','0')->orWhere(function($q2){
				$q2->where('status','3')->has('visiter','<','1');
			});
		});

	
		return $agenda->whereInstitute_id($this->id)->get();

	}

	public function getCityStateAttribute()
	{
		return $this->name.' - '.$this->city->state->uf;
	}

	public function scopeActive($query)
	{
		return $query->where('active','1');
	}

	public function scopeGetByVisitorRangeDate($query, $from, $to){
		return $query->join('agendas', 'institutes.id', '=', 'agendas.institute_id')
		->join('visiters', 'agendas.id','=', 'visiters.agenda_id')
		->where('visiters.created_at','>=',$from)
		->where('visiters.created_at','<=',$to)
		->select('institutes.*');
	}

	public static function boot(){
		parent::boot();

		static::deleted(function($institute)
		{
			$institute->managers()->delete();
			$institute->agendas()->delete();
		});
	}
}
