<?php

class Userdata extends \Eloquent {
	protected $fillable = ['tel', 'username', 'cel', 'cpf', 'city_id', 'profession_id', 'cab','cab_num','address','cep','fb_profile'];

	//protected $touches = array('user');

	public static $rules = [
		'tel'       => 'required', 
		'username'  => 'required|alpha_spaces',
		'email'     => 'email|unique:users',
		'cel'       => 'required', 
		'cpf'       => 'required|unique:userdatas', 
		'city_id'    => 'required',
		'state_id'   => 'required',
		'address'   => 'required',
		'cep'   => 'required',
		'cab'       => 'required',
		'birthday' => 'required|date_format:"d/m/Y"',
		'profession_id'   => 'required',
		'terms'       => 'required', 
		'src'       => 'image', 
	];
	public static $edit_rules = [
		'tel'       => 'required', 
		'username'  => 'required',
		'address'   => 'required',
		'cep'   => 'required',
		'cel'       => 'required', 
		'city_id'    => 'required',
		'state_id'   => 'required',
		'cab'       => 'required',
		'profession_id'   => 'required',
		'src'       => 'image', 
	];

	public $timestamps = false;

	public function user(){
		return $this->belongsTo('User');
	}

	public function city(){
		return $this->belongsTo('City');
	}

	public function profession()
	{
		return $this->belongsTo('Profession');
	}

	
}