<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Agendamento Caravanas Administração - Administração</title>
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/admin.css') }}

	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/jquery-ui.min.js') }}
</head>
<body>
	@yield('content')

	{{ HTML::script('js/bootstrap-select.min.js') }}
	{{ HTML::script('js/jquery.validate.js') }}
	{{ HTML::script('js/dropzone.js') }}
	{{ HTML::script('js/jquery.mask.js') }}
	{{ HTML::script('js/scripts.js') }}
</body>
</html>