<footer class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	<div class="content">
		<div class="row">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
				<p><a href="http://belezanatural.com.br/"><i class="fa fa-angle-left"></i> Voltar para o site do BN</a></p>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<img class="logo-endevor hide-mobile pull-right" src="{{ asset('img/logo empreendedor endeavor.png') }}">
			</div>
		</div>

		<div class="row direitos">
			<div  class="pull-left">
				Copyright © 2012 - Instituto Beleza Natural
			</div>
			<div  class="pull-right">
				Desenvolvido por DIZ'AIN
			</div>
		</div>

	</div>
</footer>