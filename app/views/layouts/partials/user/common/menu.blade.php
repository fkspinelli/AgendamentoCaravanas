<div class="menu-perfil">
	<div class="clearfix ola-usuario">
		<div class="pull-left" style="padding-right: 10px;">
			{{ $user->getImage() }}
		</div>
		<div class="pull-left" style="padding-top: 90px;">
			<h3 class="white">Olá {{ $user->userdata->username }}.</h3>
				<p><a href="{{ route('user.front.edit') }}" class="amarelo">Editar meu perfil</a></p>

		</div>

	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="buttons-perfil">
				
				@if(Sentry::getUser()->hasAccess('colaborator'))
					<a href="{{ route('agendamento.create') }}" class="btn btn-roxo">AGENDAMENTO</a>
					<a href="{{ route('agendamento.index') }}" class="btn btn-roxo">CARAVANAS AGENDADAS</a>
					@if(Sentry::getUser()->email == 'promocional@belezanatural.com.br')
						<a href="{{ route('materials.index') }}" class="btn btn-roxo">MATERIAL DE APOIO</a>
					@endif
				@else
					<a href="{{ route('institutes.leaders') }}" class="btn btn-roxo">LISTAGEM DE LÍDERES</a>
				@endif

		   	    <a href="{{ route('user.logout') }}" class="btn btn-vermelho">SAIR</a>
			</div>
		</div>
	</div>
</div>