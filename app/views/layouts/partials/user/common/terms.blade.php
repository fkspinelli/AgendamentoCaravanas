

<!-- modal termos e condições -->
<div id="termosModal" class="modal fade modal-text" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn btn-vermelho pull-right" data-dismiss="modal">Fechar</button>
        <h3 class="modal-title">Termos e Condições</h3>
      </div>
      <div class="modal-body">
      			<h2>Agendamento Online de Caravanas</h2>
				<div class="topico-termos">
					<h3>1) Objetivo do Site:</h3>
					<p>O Site consiste em centralizar e facilitar a comunicação entre as Partes: Líder de Caravana, Participante de Caravana e Instituto.</p>
				</div>
				<hr>
				<div class="topico-termos">
					<h3>2) Partes</h3>
					<p>
						Cada parte terá suas funcionalidades dentro do site <br>
						2.1 - A Líder de Caravana cadastra-se através do Cadastramento Online do site BN (http://belezanatural.com.br/) e, assim, poderá, através dessa plataforma, agendar sua caravana com o Instituto de sua preferência. É requisito essencial para se cadastrar como Líder de Caravana, estar também inscrita no Clube Amiga da Beleza. <br>
						2.2 - A Participante, ou seja, a pessoa que tenha interesse em participar de uma caravana, irá se cadastrar através do Cadastramento Online do site BN (http://belezanatural.com.br/). Assim, de acordo com sua Cidade e Estado, ela visualizará todas as Líderes disponíveis, previamente cadastradas na plataforma, e que estão próximas a ela. A referida participante visualizará os dados das Líderes de Caravana: Nome, Foto, Telefones para contato e e-mail. <br>
						2.3 - O Instituto, por sua vez, visualizará todos os agendamentos solicitados pelas Líderes que se cadastraram para irem ao mesmo, bem como as participantes da respectiva Caravana.
					</p>
				</div>
				<hr>
				<div class="topico-termos">
					<h3>3) Cessão de Direitos e de Informação</h3>
					<p>
						Destaca-se que a Líder e a participante, no momento de seu cadastro no site, estarão cedendo, sem quaisquer restrições: <br>
						3.1 - Seus dados completos, constantes do cadastro, para uso do Beleza Natural e prestadores de serviços relacionados ao Projeto. <br>
						3.2 - Com relação às Líderes de Caravanas, seus dados de Nome, Foto, Telefones para contato e e-mail, serão disponibilizados às participantes devidamente cadastradas. <br>
						3.3 – Com relação às participantes, todos os seus dados de cadastro estarão disponíveis às Lideres de Caravanas devidamente cadastradas. <br>
						3.4 - Com relação às Líderes de Caravanas e às participantes, elas cedem direito de imagem ao Beleza Natural. <br>
						3.5 - As Líderes de Caravanas e as participantes autorizam e aceitam que sejam enviados e-mails e que os colaboradores do Beleza Natural entrem em contato com elas através dos dados cadastrados no site. Página 2 de 2 4) Da Responsabilidade 
					</p>
				</div>
				<hr>
				<div class="topico-termos">
					<h3>4) Da Responsabilidade</h3>
					<p>
						É de conhecimento das Líderes de Caravanas e participantes cadastradas no site que: <br>
						4.1 – As pessoas, que se cadastrarem, responsabilizam-se pela veracidade das informações preenchidas no site. <br>
						4.2 - O Beleza Natural e seus Institutos não se responsabilizam pelo cadastramento incorreto. <br>
						4.3 – O Beleza Natural não se responsabiliza pelo transporte das pessoas e das Caravanas, por qualquer problema no caminho, tão pouco, por valores cobrados pela Líder de Caravana. <br>
						4.4 – O Beleza Natural não tem qualquer responsabilidade por transtornos pessoais ocorridos pelo cadastramento e disponibilidade das informações cadastrais lançadas no sistema. 
					</p>
				</div>
				<hr>
				<div class="topico-termos">
					<h3>5) Disposições Gerais</h3>
					<p>
						5.1 - As Líderes de Caravana e as participantes, uma vez cadastradas, declaram que estão se inscrevendo voluntariamente e que não possuem nenhum vínculo empregatício com o Beleza Natural. <br>
						5.2 - Fica, desde já, estabelecido que, para a aplicação dos serviços de Super-Relaxante e de Cachuá, todas as participantes que forem ao Instituto Beleza Natural pela primeira vez, ou retornarem após um período mínimo de 3 (três) meses, deverão realizar a chamada Avaliação Capilar que visa a análise da saúde do cabelo. Caso a cliente não esteja apta, a livre critério do Beleza Natural, esse serviço não poderá ser realizado e o Beleza Natural não será responsável por qualquer despesa que a participante tenha tido para ir até o Instituto. <br>
						5.3 - A Líder das Caravanas organiza e orienta o grupo desde a saída da Cidade de origem a um dos Institutos destinos, até o retorno das participantes para casa; informando sobre serviços, produtos e promoções do Beleza Natural. <br>
						5.4 - A participação neste Programa de Agendamento Online não gera às participantes nenhum outro direito e/ou vantagem que não estejam expressamente previstos neste Termos e Condições. <br>
						5.5 - Fica, desde já, eleito o foro da Comarca da Cidade do Rio de Janeiro/RJ, para solução de quaisquer questões referentes ao presente Termos e Condições.

					</p>
					<br>
					<p>
						Rio de Janeiro/RJ, 18 de março de 2016 <br>
						<b>COR BRASIL INDÚSTRIA E COMÉRCIO S/A</b>
					</p>
					<br>
				</div>
      </div>
    </div>

  </div>
</div>
