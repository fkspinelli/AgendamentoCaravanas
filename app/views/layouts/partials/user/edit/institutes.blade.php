<legend class="roxo">2. Instituto(s) da minha Caravana:</legend>
<div class="row"> 	

	@foreach($institutes as $k => $institute)
		<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<button type="button" class="btn btn-white " data-toggle="collapse" data-target="#collapse-{{ $k }}">{{ State::find($k)->name }}<i class="fa fa-plus fa-right"></i></button>
				<div id="collapse-{{ $k }}" class="collapse box-collapse">
					<div class="container-collapse">
						@foreach($institute as $i)
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<label class="checkbox-inline">
								
								{{ Form::checkbox('institutes[]', $i->id, in_array($i->name, $user->institutes->lists('name','id')) ) }}{{ $i->name }}
							</label>
						</div>
						@endforeach
					</div>
				</div>
		</div>
	@endforeach
</div>
