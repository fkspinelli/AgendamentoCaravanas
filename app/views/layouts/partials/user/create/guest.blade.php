<div class="desenho-2"></div>
	<section>
		<div class="menu-perfil">
			<div class="row ola-usuario cadastro-lider col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h2 class="roxo st_ryde_italicitalic size45">Quero ser participante!</h2>
				<p class="vinho size18">Venha se tornar uma belezete e esbanje autoestima e cachos perfeitos por aí!</p>
			</div>
		</div>

		<div class="row box">
			<div class="regras-beneficios container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Benefícios de ser participante</h3>
			 	</div>
			 	<div class="container">
			 		<div class="box-beneficios">
					  	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					  		<img src="{{ asset('img/CadastroParticipante_03.jpg') }}" width="130px">
					  		<p class="roxo">No valor atual do Super-Relaxante, de quinta a sábado.</p>
					  	</div>
					  	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					  		<img src="{{ asset('img/CadastroParticipante_04.jpg') }}" width="130px">
					  		<p class="rosa">No valor atual do Super-Relaxante, de segunda a quarta.</p>
					  	</div>
					  	<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					  		<img src="{{ asset('img/CadastroParticipante_05.jpg') }}" width="130px">
					  		<p class="laranja">No valor atual dos kits de todas as linhas de produtos.</p>
					  	</div>
				  	</div>
			 	</div>
			 	<div class="beneficios col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 		<p>Apenas as clientes inscritas no Clube Amiga da Beleza irão usufruir das promoções. Os descontos das caravanas, não são cumulativos com outras promoções do Beleza Natural e possuem validade determinada.</p>
			 	</div>
			</div>
		</div>