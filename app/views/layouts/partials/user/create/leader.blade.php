<div class="desenho-2"></div>
	<section>
		<div class="menu-perfil">
			<div class="row ola-usuario cadastro-lider col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<h2 class="roxo st_ryde_italicitalic size45">Quero ser líder!</h2>
				<p class="vinho size18">Ser líder é espalhar autoestima e cachos perfeitos com o Beleza Natural e ainda usufruir de benefícios exclusivos.</p>
			</div>
		</div>

		<div class="row box">
			<div class="regras-beneficios container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Conheça as regras e benefícios</h3>
			 	</div>
			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 		<div class="box-regras">
					  	<div class="regras bg-rosa container">
					  		<p><b class="roxo">1.</b> Para ser considerada caravana, o grupo deverá ser formado por, no mínimo, 10 (dez) clientes durante todos os dias da semana.</p>
					  	</div>
					  	<div class="regras bg-amarelo container">
					  		<p><b class="roxo">2.</b> Para se cadastrar como Lider de Caravana, entre no site: belezanatural.com.br/<br>caravanas</p>
					  	</div>
					  	<div class="regras bg-roxo container">
					  		<p><b class="amarelo">3.</b> Apenas as clientes de Caravanas inscritas no Clube Amiga da Beleza irão usufruir dos benefícios e descontros de Caravanas.</p>
					  	</div>
					  	<div class="regras bg-vermelho container">
					  		<p><b class="laranja">4.</b> A Caravana deve ter apenas uma Lider que precisará agendar sua visita com um instituto, de preferência, com no mínimo 3 dias de antecedencia.</p>
					  	</div>
				  	</div>
			 	</div>
			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 		<div class="lista-beneficios">
			 			<div class="row">
			 				<div class="col-sm-6 clearfix">
			 					<div class="circle bg-d54060">
			 						<span>traga</span>
			 						<span>10</span>
			 						<span>clientes</span>
			 					</div>
			 					<div class="list">
			 						<h4 class="cl-d54060">Ganhe</h4>
			 						<ul>
			 							<li>01 serviço à escolha da líder</li>
			 						</ul>
			 					</div>
			 				</div>
			 				<div class="col-sm-6 clearfix">
			 					<div class="circle bg-820a37">
			 						<span>traga</span>
			 						<span>14</span>
			 						<span>clientes</span>
			 					</div>
			 					<div class="list">
			 						<h4 class="cl-820a37">Ganhe</h4>
			 						<ul>
			 							<li>01 serviço à escolha + 01 Kit de produtos à escolha</li>
			 						</ul>
			 					</div>
			 				</div>
			 			</div>
			 			<div class="row">
			 				<div class="col-sm-6 clearfix">
			 					<div class="circle bg-f1ab27">
			 						<span>traga</span>
			 						<span>30</span>
			 						<span>clientes</span>
			 					</div>
			 					<div class="list">
			 						<h4 class="cl-f1ab27">Ganhe</h4>
			 						<ul>
			 							<li>01 serviço à escolha + 02 Kits de produtos à escolha</li>
			 						</ul>
			 					</div>
			 				</div>
			 				<div class="col-sm-6 clearfix">
			 					<div class="circle bg-6d0a52">
			 						<span>traga</span>
			 						<span>45</span>
			 						<span>clientes</span>
			 					</div>
			 					<div class="list">
			 						<h4 class="cl-6d0a52">Ganhe</h4>
			 						<ul>
			 							<li>02 serviços à escolha + 02 Kits de produtos à escolha</li>
			 						</ul>
			 					</div>
			 				</div>
			 			</div>
			 			<div class="row">
			 				<div class="col-sm-6 clearfix">
			 					<div class="circle bg-d54060">
			 						<span>traga</span>
			 						<span>60</span>
			 						<span>clientes</span>
			 					</div>
			 					<div class="list">
			 						<h4 class="cl-d54060">Ganhe</h4>
			 						<ul>
			 							<li>03 serviços à escolha + 02 Kits de produtos à escolha</li>
			 						</ul>
			 					</div>
			 				</div>
			 				<div class="col-sm-6">
			 					
			 				</div>
			 			</div>
			 		</div>
			 	</div>
			</div>
		</div>