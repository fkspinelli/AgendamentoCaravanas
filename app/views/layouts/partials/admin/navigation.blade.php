        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ route('admin.institutos.index') }}"><img height="30" src="{{asset('img/logo_beleza_natural.png')}}" alt=""></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
         
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Sentry::getUser()->username }} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li>
                            @if(Sentry::getUser()->hasAccess('admin'))
                             <a href="{{route('admin.logout')}}">Sair</a>
                             @else
                             <a href="{{route('manager.logout', Route::current()->getParameter('slug'))}}">Sair</a>
                             @endif
                       </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               @if(Sentry::getUser()->hasAccess('manager')) 
                    @include('layouts.partials.admin.navigation-bar-manager', ['slug'=>$slug]) 
               @else
                    @include('layouts.partials.admin.navigation-bar-admin')
               @endif
            </div>
            <!-- /.navbar-collapse -->
        </nav>