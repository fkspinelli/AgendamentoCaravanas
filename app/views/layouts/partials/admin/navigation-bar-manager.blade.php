 <ul class="nav navbar-nav side-nav">
                   
     <li class="active">
        <a href="{{ route('instituto.agendas.index', $slug) }}"><i class="fa fa-fw fa-book"></i> Agendas</a>
    </li>
    <li class="active">
        <a href="{{ route('admin.agendamento.create', $slug) }}"><i class="fa fa-fw fa-book"></i> Agende uma caravana</a>
    </li>
     <li class="active">
        <a href="{{ route('instituto.usuarios.index', $slug) }}"><i class="fa fa-fw fa-user"></i> Líderes</a>
    </li>
     <li class="active">
        <a href="{{ route('visiters.pendentes', $slug) }}"><i class="fa fa-fw fa-user"></i> Pós visitas</a>
    </li>
    
</ul>