 <ul class="nav navbar-nav side-nav">
    <li class="active">
        <a href="{{ route('admin.agendas.index') }}"><i class="fa fa-fw fa-book"></i> Agendas</a>
    </li>  
   
    <li class="active">
        <a href="{{ route('admin.usuarios.index') }}"><i class="fa fa-fw fa-user"></i> Líderes</a>
    </li>
     <li class="active">
        <a href="{{ route('admin.usuarios.guests') }}"><i class="fa fa-fw fa-users"></i> Participantes</a>
    </li>          
    <li class="active">
        <a href="{{ route('admin.institutos.index') }}"><i class="fa fa-fw fa-pencil-square-o"></i> Institutos</a>
    </li>
    <li class="active">
        <a href="{{ route('admin.materiais.index') }}"><i class="fa fa-fw fa fa-file"></i> Material de Apoio</a>
    </li>

    <li class="active">
        <a href="{{ route('admin.relatorios.conversion', ['from'=>Helper::ConverterBR(Helper::SubtrairMeses(date('Y-m-d'), 1),true), 'to'=>date('d/m/Y')]) }}"><i class="fa fa-fw fa-dashboard"></i> Relatórios</a>
    </li>

     <li class="active">
        <a href="{{ route('admin.users') }}"><i class="fa fa-fw fa-dashboard"></i> Usuários Admins</a>
    </li>
 
</ul>