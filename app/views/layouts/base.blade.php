<!DOCTYPE>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Agendamento Caravanas - Beleza Natural</title>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/style.css') }}

	{{ HTML::script('js/jquery.js') }}
	{{ HTML::script('js/bootstrap.min.js') }}
	{{ HTML::script('js/jquery-ui.min.js') }}
</head>
<body>
	
	@include('layouts.partials.header')

	
	@yield('content')

			
	@include('layouts.partials.footer')
	

	
	
	{{ HTML::script('js/bootstrap-select.min.js') }}
	{{ HTML::script('js/jquery.validate.js') }}
	{{ HTML::script('js/dropzone.js') }}
	{{ HTML::script('js/jquery.mask.js') }}
	{{ HTML::script('js/scripts.js') }}

</body>
</html>