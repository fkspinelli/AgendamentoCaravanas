@section('content')
<div class="desenho"></div>
<section>
	<div class="row">
		<div class="box-cadastro container col-lg-7 col-md-7 col-sm-12 col-xs-12">
		 	<div class="box-cadastro-header">
		 		<h3>Solicitação de Agendamento</h3>
		 	</div>
		 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 		<h4>Seu pedido de agendamento foi enviado com sucesso!</h4>
		 		<p>O Instituto verificará a disponibilidade da data para confirmação deste agendamento.</p>
				
		 		<a href="{{ route('agendamento.create') }}" class="truncate btn btn-danger col-lg-8 col-md-8 col-sm-8 col-xs-12" style="font-size: 16px;">Quero fazer outro agendamento</a>
		 		<a href="{{ route('user.front.edit') }}" style="position: relative; top: 10px; left: 10px; ">Voltar para meu perfil</a>
		 	</div>
		</div>
	</div>

</section>
@stop