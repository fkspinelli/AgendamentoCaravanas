@section('content')
<div class="desenho"></div>
<section>
	<div class="row">
		<div class="box-cadastro container col-lg-7 col-md-7 col-sm-12 col-xs-12">
		 	<div class="box-cadastro-header">
		 		<h3>Cadastro realizado com sucesso!</h3>
		 	</div>
		 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 		@include('layouts.notifications')
		 		@if(!Sentry::getUser()->hasAccess('colaborator'))
		 		<a href="{{ route('institutes.leaders') }}" class="truncate btn btn-danger col-lg-9 col-md-10 col-sm-12 col-xs-12" style="font-size: 16px;">Encontre agora a sua caravana</a>
		 		@else
		 		<a href="{{ route('agendamento.create') }}" class="truncate btn btn-danger col-lg-9 col-md-10 col-sm-12 col-xs-12" style="font-size: 16px; margin-top: 10px;">Faça já o seu primeiro agendamento</a>
		 		@endif
		 	</div>
		</div>
	</div>

</section>
@stop