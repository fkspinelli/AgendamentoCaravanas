@section('content')
	{{-- Conteudo da home  
		** header - /views/layouts/partials/header
		** footer - /views/layouts/partials/footer
	--}}
	<!-- <img id="desenho" src="../img/caravanas-img.png"> -->
	<div class="desenho"></div>
	<section>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<h4 class="resposta-recuperar-senha">Esqueceu o seu login ou seu email cadastrado?<br>
				Digite abaixo o seu CPF e receba um email para redefinir sua senha. 
				</h4>
			</div>
		</div>

		<div class="row">
			<div class="box-cadastro resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Recuperar senha ou e-mail</h3>
			 	</div>

			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 		@include('layouts.notifications')
				  	{{ Form::open(['route'=>'password.request']) }}
						<div class="form-group">
							<input type="text" class="form-control" value="{{ Input::old('cpf') }}" id="cpf" name="cpf" placeholder="Digite seu CPF">
						</div>
						<button type="submit" class=" truncate btn btn-danger col-lg-2 col-md-3 col-sm-2 col-xs-12 pull-right" style="font-size: 16px;">Enviar</button>
				  	{{ Form::close() }}

				  	<!-- ***Mesagem de erro*** -->
<!-- 				  	<p class="msg-erro col-lg-10 col-md-9 col-sm-10 col-xs-12" style="margin-left:-15px; margin-right:15px;">Este CPF não está cadastrado em nosso banco de dados, favor inserir um CPF válido.</p>
			 		<button type="button" class=" truncate btn btn-danger col-lg-2 col-md-3 col-sm-2 col-xs-12" style="font-size: 16px;">Enviar</button> -->
			 	
			 	</div>

			 	<!-- ***Resposta envio de senha*** -->
			 	<!-- <div class="container col-lg-9 col-md-9 col-sm-12 col-xs-12">
			 		<h4>Solicitação enviada com sucesso</h4>
			 		<p>O seu e-mail cadastrado é: <span class="laranja">isab...@dizain.com.br</span>
			 		<br>Clique no link que foi enviado para o seu e-mail e crie uma nova senha.</p>
			 	</div> -->

			</div>
		</div>
	
	</section>
@stop