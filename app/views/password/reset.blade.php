@section('content')
	{{-- Conteudo da home  
		** header - /views/layouts/partials/header
		** footer - /views/layouts/partials/footer
	--}}
	<!-- <img id="desenho" src="../img/caravanas-img.png"> -->
	<div class="desenho"></div>
	<section>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<h4 class="resposta-recuperar-senha">Digite abaixo a sua nova senha que deve conter no mínimo 4 caracteres.</h4><br>
			</div>
		</div>
		<div class="row">
			<div class="box-cadastro resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Redefinir senha</h3>
			 	</div>

			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
			 		@include('layouts.notifications')
				  	{{ Form::open(['route'=>'password.reset']) }}
				  	{{ Form::hidden('token', $token) }}
						<div class="form-group">
							<input type="password" name="password" class="form-control"  placeholder="Digite sua nova senha">
						</div>
						<div class="form-group">
							<input type="password" name="password_confirmation" class="form-control" placeholder="Digite novamente sua nova senha">
						</div>
					  	<p class="col-lg-10 col-md-9 col-sm-10 col-xs-12" style="margin-left:-15px; margin-right:15px;"></p>
				 		<button type="submit" class=" truncate btn btn-danger col-lg-2 col-md-3 col-sm-2 col-xs-12" style="font-size: 16px;">Enviar</button>
				  	{{ Form::close() }}
			 	</div>

			 	<!-- *** Resposta senha redefinida *** -->
			 <!-- 	<div class="container col-lg-9 col-md-12 col-sm-12 col-xs-12">
			 		<h4>Sua senha foi redefinida com sucesso!<br>
			 			Clique no botão abaixo para entrar no site da Caravana da Beleza.</h4>
			 		<button type="button" class=" truncate btn btn-danger col-lg-6 col-md-6 col-sm-6 col-xs-12" style="font-size: 16px;">Acessar meu perfil</button>
			 	</div> -->

			</div>
		</div>
	
	</section>
@stop