@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Relatórios {{ $institutes->count() }}
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row" id="searchBlock">
    <div class="col-md-10">

      <div class="row">
        <div class="col-md-8">
          <p>Filtrar por:</p>

          {{ (Input::has('from') && Input::has('to')) ?  '<p><strong>No período de '.Input::get('from').' até '.Input::get('to').'</strong></p>' : null }}

        </div>
        <div class="col-md-4 text-right">
          {{ Form::open(['route'=>'admin.relatorios.conversion.post']) }}
            {{ Form::hidden('state_id', Input::get('state_id')) }}
            {{ Form::hidden('institute_id', Input::get('institute_id')) }}
            {{ Form::hidden('from', Input::get('from')) }}
            {{ Form::hidden('to', Input::get('to')) }}
             {{ Form::button('<i class="fa fa-file-excel-o"></i> Relatorios', ['type'=>'submit', 'class'=>'btn btn-info']) }}
          {{ Form::close() }}
        </div>
      </div>
      {{ Form::open(['route'=>'admin.relatorios.conversion', 'method'=>'get']) }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
               {{ Form::select('state_id', [''=>'Selecione estado']+$states, Input::get('state_id'), ['class'=>'form-control']) }}
          </div>
          <div class="form-group">
            {{ Form::select('institute_id', [''=>'Selecione Instituto']+$ins, Input::get('institute_id'), ['class'=>'form-control']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
               {{ Form::text('from', Input::get('from'), ['id'=>'from','class'=>'form-control','placeholder'=>'Data início']) }}
          </div>
          <div class="form-group">
             {{ Form::text('to', Input::get('to'), ['id'=>'to','class'=>'form-control','placeholder'=>'Data término']) }}
          </div>
           <div class="form-group text-right">

            {{ Form::button('<i class="fa fa-search"></i> Filtrar', ['class'=>'btn btn-primary','type'=>'submit']) }}
            </div>
        </div>
      </div>

        {{ Form::close() }}
    </div>
    <div class="col-md-2">
        {{ Form::open(['route'=>'relatorio.all']) }}
        <div class="form-group">
          {{ Form::select('period', [''=>'Selecione período para consolidados', '1'=>'Último mês', '3'=>'3 meses', '6'=>'6 meses'], Input::old('period'), ['class'=>'form-control']) }}
        </div>
        <div class="form-group">
          {{ Form::button('<i class="fa fa-file-excel-o"></i> Consolidados', ['type'=>'submit', 'class'=>'btn btn-success btn_loader']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>
<div class="col-md-12">

    @include('layouts.notifications')

    @if($institutes->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
            <tr>
                <th>Instituto</th>
               <th class="text-center">Agendamentos solicitados</th>
               <th class="text-center">Agendamentos confirmados</th>
               <th class="text-center">Agendamentos realizados</th>
               <th class="text-center">Agendamentos não realizados</th>
               <th class="text-center">Conversões de visitas<br/><small>(Formularios pós-visitas preenchidos)</small></th>
               <th class="text-center">Taxa de conversão</th>

            </tr>
            @foreach($institutes as $institute)
                <tr>
                  <td><strong>{{ $institute->name }}</strong></td>
                  <td align="center">
                      {{ $institute->getAgendasCount(0, Input::get('from', null), Input::get('to', null)) }}
                  </td>
                   <td align="center">
                      {{ $institute->getAgendasCount(1, Input::get('from', null), Input::get('to', null)) }}
                  </td>
                   <td align="center">
                      {{ $institute->getAgendasCount(3, Input::get('from', null), Input::get('to', null)) }}
                  </td>
                  <td align="center">

                      <p><strong>Total:</strong> {{ ($institute->getAgendasCount(2, Input::get('from', null), Input::get('to', null))+$institute->getWithoutVisitorsCount()+$institute->getAgendasCount(0, Input::get('from', null), Input::get('to', null))) }}</p>
                      <p>Cancelado: {{$institute->getAgendasCount(2, Input::get('from', null), Input::get('to', null))}}</p>
                      <p>Não preechido: {{$institute->getWithoutVisitorsCount()}}<br/>
                      <p>Aguardando: {{$institute->getAgendasCount(0, Input::get('from', null), Input::get('to', null))}}</p>

                      @if(($institute->getAgendasCount(2, Input::get('from', null), Input::get('to', null))+$institute->getWithoutVisitorsCount()) > 0)
                      <a data-toggle="modal" data-target="#modal_inst_{{ $institute->id }}" class="btn btn-warning" href=""><i class="fa fa-external-link-square"></i></a>
                        <!-- Modal -->
                        <div id="modal_inst_{{ $institute->id }}" class="modal fade" role="dialog">
                          <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Agendamentos não realizados</h4>
                              </div>
                              <div class="modal-body">

                                  @foreach($institute->getNotRealized(Input::get('from', null), Input::get('to', null)) as $agenda)
                                  <div style="border-bottom:2px dotted #c4c4c4; padding-bottom:10px; margin-bottom:10px;">
                                    <p><strong>Responsável pelo agendamento:</strong> {{ $agenda->user->userdata->username or "--" }}</p>
                                    <p><strong>Email:</strong> {{ $agenda->user->email or "--" }}</p>
                                    <p><strong>Tel:</strong> {{ $agenda->user->userdata->tel or "--" }} {{ $agenda->user->userdata->cel or "--" }}</p>
                                    <p><strong>Status do agendamento:</strong> {{ $agenda->statusText() }}</p>
                                    <p>Data: {{$agenda->created_at}}</p>
                                    @if($agenda->visiter()->count() < '1' and $institute->getAgendasCount(3, Input::get('from', null), Input::get('to', null)) > '0')
                                      <p class="alert alert-danger">Não preencheu o cadastro pós-visita</p>
                                    @endif

                                  </div>
                                  @endforeach

                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                              </div>
                            </div>

                          </div>
                        </div>
                    @endif
                  </td>
                  <td align="center">
                      {{ $institute->visiters->count() }}


                      @if($institute->visiters->count() > 0)
                        <!-- Formulario para geração de relatorio de pós-visitas -->


                        {{ Form::open(['route'=>'admin.relatorios.posvisitas']) }}
                        {{ Form::hidden('institute_id', $institute->id) }}
                        {{ Form::button('<i class="fa fa-file-excel-o"></i>', ['type'=>'submit', 'class'=>'btn btn-success']) }}
                        {{ Form::close() }}
                      @endif
                  </td>
                  <td align="center">
                      {{ $institute->getAgendasCount(3) != '0' ? round(($institute->getVisitorsCount()*100)/$institute->getAgendasCount(3)) : '0' }}%
                  </td>
                </tr>
            @endforeach
        </table>
        {{ $institutes->appends($_GET)->links() }}
    </div>
    @else
    <div class="alert alert-warning">
        <p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
    </div>
    @endif
</div>
@stop
