@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            {{ $alias }} <small>({{ $users->getTotal() }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row" id="searchBlock">
	<div class="col-md-10">
		{{ Form::open(['route'=>'users.search', 'method'=>'get', 'class'=>'form-inline']) }}
		Filtrar por:
		{{ Form::select('state_id', [''=>'Selecione Estado']+$states, Input::get('state_id'), ['class'=>'form-control','id'=>'state_id']) }}
		{{ Form::select('city_id', [''=>'Selecione Cidade']+$cities, Input::get('city_id'), ['class'=>'form-control','id'=>'city_id']) }}
    {{ Form::select('activity', [''=>'Selecione por atividade', '0' => 'Ativo', '1'=>'Inativo'], Input::get('activity'), ['class'=>'form-control']) }}
    {{ Form::button('<i class="fa fa-search"></i> Buscar', ['class'=>'btn btn-primary','type'=>'submit']) }}
		{{ Form::close() }}
	</div>
	<div class="col-md-2">
		{{ Form::open(['route'=>'user.download.excel']) }}
		{{ Form::hidden('alias',$alias) }}
		@foreach($users_all as $u)
		{{ Form::hidden('users[]', $u->id) }}
		@endforeach
		{{ Form::button('<i class="fa fa-file-excel-o"></i> Gerar relatório', ['class'=>'btn btn-success','type'=>'submit']) }}
		{{ Form::close() }}
	</div>
</div>
<div class="col-md-12">

	@include('layouts.notifications')

	@if($users->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Telefones</th>
				<th>Local</th>
				@if($alias == 'Líderes')
				<td>Último agendamento realizado</td>
				<td>Institutos de interesse</td>
				@endif
				<th>Ativo/Inativo</th>
				<th>Apagar</th>

			</tr>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->userdata->username }}</td>
					<td>{{ $user->userdata->cpf }}</td>
					<td>{{ $user->userdata->cel }} | {{ $user->userdata->cel }}</td>
					<td>{{ $user->userdata->city->name }} - {{ $user->userdata->city->state->uf }}</td>
					@if($alias == 'Líderes')
					<td>
						@if($user->agendas->count()>0)
						{{ Helper::ConverterBR($user->agendas->last()->created_at) }} às {{ Helper::Hora($user->agendas->last()->created_at) }}
						@endif
					</td>
					<td>
						<ul>
							@foreach($user->institutes as $i)
								<li>{{ $i->name }}</li>
							@endforeach
						</ul>
					</td>
					@endif
					<!--<td>
						<a href="{{ route('admin.usuarios.edit', $user->id) }}"><i class="fa fa-edit"></i></a>
					</td>-->
					<td>
						@if($user->lead_inactive == '0')
							<i class="fa fa-check"></i> Ativo
						@else
							<i class="fa fa-close"></i> Inativo
						@endif
					</td>
					<td>
						{{ Form::open(['route'=>array('admin.usuarios.destroy', 'id'=>$user->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>

						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $users->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop
