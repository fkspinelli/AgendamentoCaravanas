@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
           Usuários administrativos <small>({{ $users->getTotal() }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row" id="searchBlock">
	<div class="col-md-10">
		{{ Form::open(['route'=>'admin.users.create']) }}
		
		{{ Form::text('username', null, ['class'=>'form-group','placeholder'=>'Nome']) }}
		{{ Form::email('email', null, ['class'=>'form-group','placeholder'=>'Email']) }}
		{{ Form::password('password', ['class'=>'form-group','placeholder'=>'Senha']) }}
		{{ Form::password('password_confirmation', ['class'=>'form-group','placeholder'=>'Confirmar Senha']) }}

		{{ Form::button('Criar novo', ['class'=>'btn btn-primary','type'=>'submit']) }}
		{{ Form::close() }}
	</div>
</div>
<div class="col-md-12">
	
	@include('layouts.notifications')
	
	@if($users->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->userdata->username }}</td>
					<td>{{ $user->email }}</td>
					<td>
						{{ Form::open(['route'=>array('admin.users.delete',$user->id),'method'=>'delete']) }}
						{{ Form::submit('Apagar', ['class'=>'btn btn-danger']) }}
						{{ Form::close() }}
					</td>
					
			@endforeach
		</table>
		{{ $users->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop
