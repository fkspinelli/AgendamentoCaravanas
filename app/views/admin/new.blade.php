@extends('layouts.login')
@section('content')
	<div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            	<h1 class="text-center"><img src="{{ asset('img/logo_beleza_natural.png') }}" alt="Agendamento Caravanas - Login"></h1>
            	@include('layouts.notifications')
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro no sistema</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(['route'=>array('register.new.manager', $slug)]) }}
                        {{ Form::hidden('institute_slug', $slug) }}
                            <fieldset>
                                <div class="form-group">
                                   {{ Form::text('username', null, ['placeholder'=>'Nome', 'class'=>'form-control']) }}
                                </div>
                                <div class="form-group">
                                   {{ Form::email('email', null, ['placeholder'=>'E-mail', 'class'=>'form-control']) }}
                                </div>
                               
                                <div class="form-group">
                                   {{ Form::password('password', ['placeholder'=>'Senha', 'class'=>'form-control']) }}
                                </div>
                              
                                {{ Form::submit('Cadastrar', ['class'=>'btn btn-lg btn-success btn-block']) }}
                                
                            </fieldset>
                        {{ Form::close() }}
                        <a class="pull-right" href="{{ route('manager.index', $slug) }}"><i class="fa fa-arrow-left"></i> Voltar ao login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop