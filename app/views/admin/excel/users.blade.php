<html>
<title>Agenda</title>
<meta charset="UTF-8">
<body>
	        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Email</th>
				<th>CPF</th>
				<th>Aniversário</th>
				<th>Facebook</th>
				<th>Cep</th>
				<th>Endereço</th>
				<th>Cidade</th>
				<th>Estado</th>
				<th>Telefone</th>
				<th>Celular</th>
				<th>Profissão</th>
				<th>Ativo</th>

				<th>CAB</th>

				@if($alias == 'Líderes')
				<td>Último agendamento realizado</td>
				<td>Institutos de interesse</td>
				@endif


			</tr>
			@foreach($users as $user)
				<tr>
					<td>{{ $user->userdata->username }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->userdata->cpf }}</td>
					<td>{{ Helper::ConverterBR($user->userdata->birthday, true) }}</td>
					<td>{{ $user->userdata->fb_profile or '--'}}</td>
					<th>{{ $user->userdata->cep }}</th>
					<th>{{ $user->userdata->address }}</th>
					<td>{{ $user->userdata->city->name }}</td>
					<td>{{ $user->userdata->city->state->uf }}</td>
					<td>{{ $user->userdata->tel }}</td>
					<td>{{ $user->userdata->cel }}</td>


					<td>{{ $user->userdata->profession->name }}</td>

					<td>{{ $user->lead_inactive == '1' ? 'Inativo' : 'Ativo' }}</td>
					<td>{{ $user->userdata->cab_num or '--' }}</td>


					@if($alias == 'Líderes')
					<td>
						@if(isset($user->agendas->last()->id))
						{{ Helper::ConverterBR($user->agendas->last()->created_at) }} às {{ Helper::Hora($user->agendas->last()->created_at) }}
						@endif
					</td>
					<td>
						<ul>
							@foreach($user->institutes as $i)
								<li>{{ $i->name }}</li>
							@endforeach
						</ul>
					</td>
					@endif

				</tr>
			@endforeach
		</table>
</body>
</html>
