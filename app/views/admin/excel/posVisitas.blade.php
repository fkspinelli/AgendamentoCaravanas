<html>
<title>Agenda</title>
<meta charset="UTF-8">
<body>
	 <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Data do Agendamento</th>
				<th>Mês</th>
				<th>Ano</th>
				<th>Cidade de Origem</th>
				<th>Estado de Origem</th>
				<th>Cidade do Instituto</th>
				<th>Estado do Instituto</th>
				<th>Instituto</th>
				
				<th>Periodo que foi realizado</th>
				<th>Número de pessoas previstas para o agendamento</th>

				<th>A caravana é nova?</th>
				<th>Quantas clientes participaram da Caravana?</th>
				@foreach($questions as $q)
				<th>{{ $q->title }}</th>
				@endforeach

				<th>Líder - Nome</th>
				<th>Líder - Email</th>
				<th>Líder - CPF</th>
				<th>Líder - Tel</th>
				<th>Líder - Facebook</th>
				<th>Líder - CAB</th>
				
			</tr>
			@foreach($institute->visiters as $visiter)
				<tr>
					<td>{{ Helper::ConverterBR($visiter->created_at) }}</td>
					<td>{{ Helper::mesExtenso($visiter->created_at) }}</td>
					<td>{{ Helper::Ano($visiter->created_at) }}</td>
					<td>{{ $visiter->agenda->user->userdata->city->name or '--' }}</td>
					<td>{{ $visiter->agenda->user->userdata->city->state->uf or '--' }}</td>
					<td>{{ $visiter->institute->city->name }}</td>
					<td>{{ $visiter->institute->city->state->uf }}</td>
					<td>{{ $visiter->institute->name }}</td>
					
					<td>{{ $visiter->agenda->period->name or '--' }}</td>
					<td>{{ $visiter->agenda->num_group or '--' }}</td>

					<td>{{ $visiter->unserial()['q1'] }}</td>
					<td>{{ $visiter->unserial()['q2'] }}</td>
					
					@foreach($questions as $k => $q)

					<td>{{ $visiter->unserial()['q'.($k+2)] or null }}</td>

					@endforeach
					


					<td>{{ $visiter->agenda->user->userdata->username or '--' }}</td>
					<td>{{ $visiter->agenda->user->email or '--' }}</td>
					<td>{{ $visiter->agenda->user->userdata->cpf or '--' }}</td>
					<td>{{ $visiter->agenda->user->usedata->tel or '--' }} {{ $visiter->agenda->user->usedata->cel or '--' }}</td>
					<td>{{ $visiter->agenda->user->userdata->fb_profile or '--' }}</td>
					<td>{{ $visiter->agenda->user->userdata->cab or '--' }}</td>
						 
					
				</tr>
			@endforeach
		</table>
</body>
</html>