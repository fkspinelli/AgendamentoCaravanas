<html>
<title>Agenda</title>
<meta charset="UTF-8">
<body>
	 <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Instituto</th>
				<th>Nº de pessoas</th>
				<th>L&iacute;der responsável</th>
				<th>Cidade de Origem</th>
				<th>Estado de Origem</th>
				<th>Per&iacute;odo</th>
				<th>Data</th>
				<th>Status</th>
				
			</tr>
			@foreach($agendas as $agenda)
				<tr>
					<td>{{ $agenda->institute->name or '--' }}</td>
					<td>{{ $agenda->num_group }}</td>
					<td>{{ $agenda->user->userdata->username or '--' }}</td>
					<td>{{ $agenda->user->userdata->city->name or '--' }}</td>
					<td>{{ $agenda->user->userdata->city->state->uf or '--' }}</td>
					<td>{{ $agenda->period->name or null }}</td>

					<td>{{ Helper::ConverterBR($agenda->date_marked, true) }}</td>
					<td>{{ $agenda->statusText() }}</td>
					
				</tr>
			@endforeach
		</table>
</body>
</html>