 <html>
<title>Agenda</title>
<meta charset="UTF-8">
<body>
   <table class="table table-bordered table-hover table-striped">
            <tr>
                <th>Instituto</th>
               <th class="text-center">Agendamentos solicitados</th>
               <th class="text-center">Agendamentos confirmados</th>
               <th class="text-center">Agendamentos realizados</th>
               <th class="text-center">Agendamentos não realizados</th>
               <th class="text-center">Conversões de visitas<br/><small>(Formularios pós-visitas preenchidos)</small></th>
               <th class="text-center">Taxa de conversão</th>
                
            </tr>
            @foreach($institutes as $institute)
                <tr>
                  <td><strong>{{ $institute->name }}</strong></td>
                  <td align="center">
                      {{ $institute->getAgendasCount(0) }}
                  </td>
                   <td align="center">
                      {{ $institute->getAgendasCount(1) }}
                  </td>
                   <td align="center">
                      {{ $institute->getAgendasCount(3) }}
                  </td>
                  <td align="center">

                      Total: {{ ($institute->getAgendasCount(2)+$institute->getWithoutVisitorsCount()+$institute->getAgendasCount(0)) }}<br/>
                      Cancelado: {{$institute->getAgendasCount(2)}}<br/>
                      Não preechido: {{$institute->getWithoutVisitorsCount()}}<br/>
                      Aguardando: {{$institute->getAgendasCount(0)}}<br/>

                    
                  </td>
                  <td align="center">
                      {{ $institute->visiters->count() }}
                     
                    
                    
                  </td>
                  <td align="center">
                      {{ $institute->getAgendasCount(3) != '0' ? round(($institute->getVisitorsCount()*100)/$institute->getAgendasCount(3)) : '0' }}%
                  </td>
                </tr>
            @endforeach
        </table>
</body>
</html>
