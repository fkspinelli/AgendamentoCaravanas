@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Materiais <small>Criar novo</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	
	 @include('layouts.notifications')
			
		{{ Form::open(['route'=>'admin.materiais.store', 'files'=>true]) }}
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::label('name','Nome') }}
						{{ Form::text('name', null, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::label('description','Descrição') }}
						{{ Form::text('description', null, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::label('src','Arquivo') }}
						{{ Form::file('src') }}
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 text-right">
					{{ Form::submit('Criar novo Material',['class'=>'btn btn-primary']) }}
				</div>
			</div>



			
		{{ Form::close() }}
	
</div>
@stop
