@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Editar material <small>{{ $material->name }}</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	
	 @include('layouts.notifications')
			
		{{ Form::open(['route'=>array('admin.materiais.update', $material->id), 'method'=>'put', 'files'=>true]) }}
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::label('name','Nome') }}
						{{ Form::text('name', $material->name, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::label('description','Descrição') }}
						{{ Form::text('description', $material->description, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">

						<a href="/uploads/material/{{ $material->src }}" target="_blank" download="{{ $material->name }}" style="margin-bottom: 10px; display: block;">
							<img src="{{asset('img')}}/icon-{{explode('.', $material->src)[1]}}.png" width="30">
						</a>
						<div></div>

						{{ Form::file('src') }}</td>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 text-right">
					{{ Form::submit('Editar Material',['class'=>'btn btn-primary']) }}
				</div>
			</div>



			
		{{ Form::close() }}
	
</div>
@stop
