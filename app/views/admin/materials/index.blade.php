@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Materiais <small>({{ $materials->getTotal() }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.materiais.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($materials->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>&nbsp;</th>
				<th>Nome</th>
				<th>Descrição</th>
				<th>Editar</th>
				<th>Apagar</th>
			</tr>
			@foreach($materials as $m)
				<tr>
					<td>
						<a href="/uploads/material/{{ $m->src }}" target="_blank" download="{{ $m->name }}">
							<img src="{{asset('img')}}/icon-{{explode('.', $m->src)[1]}}.png" width="30">
						</a>
					</td>
					<td>{{ $m->name }}</td>
					<td>{{ $m->description }}</td>
					<td><a href="{{ route('admin.materiais.edit', ['id'=>$m->id]) }}"><i class="fa fa-edit"></i></a></td>
					<td>
						{{ Form::open(['route'=>array('admin.materiais.destroy', 'id'=>$m->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $materials->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop
