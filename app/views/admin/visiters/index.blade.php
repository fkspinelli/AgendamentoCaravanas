@extends('layouts.admin') 
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro pós visita 
            @if(strpos($route, 'realizados')) 
            <small>Realizados</small>
            @else
			  <small>Pendentes</small>
            @endif
        </h1>
    </div>
</div>
<div class="row" id="searchBlock">
    <div class="col-md-8">
    	{{ Form::open(['route'=>array('visiters.search', Route::current()->getParameter('slug')), 'method'=>'get', 'class'=>'form-inline']) }}
		{{ Form::text('cpf', Input::get('cpf', null), ['class'=>'form-control', 'placeholder'=>'Buscar líder por CPF']) }}
		{{ Form::submit('Buscar', ['class'=>'btn btn-primary']) }}
    	{{ Form::close() }}
    </div>
    <div class="col-md-4 text-right">
    	@if(strpos($route, 'realizados'))
    	<a class="btn btn-danger" href="{{ route('visiters.pendentes', Route::current()->getParameter('slug')) }}">Selecionar os pendentes</a>
    	@else
		<a class="btn btn-danger" href="{{ route('visiters.realizados', Route::current()->getParameter('slug')) }}">Selecionar os realizados</a>
    	@endif
    	{{--
    	{{ Form::open() }}
		{{ Form::button('<i class="fa fa-file-o"></i> Exportar', ['type'=>'submit','class'=>'btn btn-success']) }}
    	{{ Form::close() }}
    	--}}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('layouts.notifications') 
        <div class="table-responsive">
        	@if($users->count() > 0)
        	<table class="table table-striped">
            <tr>
                <th>Instituto</th>
                <th>Nº de pessoas</th>
                <th>Período</th>
                <th>Data</th>
                <th>Status</th>
                <th>Líder</th>
            </tr>
            @foreach($agendas as $agenda)
            <tr>
                    <tr>
                        <td>{{ $agenda->institute->name or null }}</td>
                        <td>{{ $agenda->num_group }}</td>
                        
                        <td>{{ $agenda->period->name }}</td>
                        <td>{{ Helper::ConverterBR($agenda->date_marked, true) }}</td>
                        <td>
                            {{ $agenda->statusText() }}
                            @if($agenda->status == '1')
                                <p><a href="">Alterar para Realizado</a></p>
                        
                            @elseif($agenda->status == '3' and !isset($agenda->visiter->id))
                                <span style="margin:10px 0; display:block;" class="alert alert-danger">Pós visita pendente</span>
                                <p><a href="{{route('visiters.create',['slug'=>Route::current()->getParameter('slug'), 'agenda_id'=>$agenda->id])}}">Preencher fomulário pós visita.</a></p>
                            @elseif($agenda->status == '3' and isset($agenda->visiter->id))
                                <span style="margin:10px 0; display:block;" class="alert alert-info">Pós visita realizado</span>
                                <p><a href="{{route('visiters.show',['id'=>$agenda->visiter->id])}}">Visualizar pós visita <i class="fa fa-arrow-right"></i></a></p>
                            @else
                            
                            @endif
                            
                        </td>
                        <td>

                            <p>{{ $agenda->user->userdata->username or null }}</p>
                            <p>{{ $agenda->user->email or null }}</p>
                            <p>{{ $agenda->user->userdata->cpf or null }}</p>
                            <p>{{ $agenda->user->userdata->cel or null }} | {{ $agenda->user->userdata->cel or null }}</p>
                            <p>{{ $agenda->user->userdata->city->name or null }} - {{ $agenda->user->userdata->city->state->uf or null }}</p>
                        </td>
                    </tr>
            </tr>

            @endforeach
        	
        	</table>
            {{ $agendas->appends($_GET)->links() }}
        	@endif
        </div>
    </div>
</div>
@stop
