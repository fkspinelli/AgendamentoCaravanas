@extends('layouts.admin') 
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro pós visita <small>Editar</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-6 alert alert-info">
        <h3>Dados do agendamento</h3>
        <p><strong>Instituto: </strong>{{ $visiter->agenda->institute->name or null }}</p>
        <p><strong>Nº de pessoas: </strong>{{ $visiter->agenda->num_group or null }}</p>
        <p><strong>Líder responsável: </strong>{{ $visiter->agenda->user->userdata->username or null }} - {{ $visiter->agenda->user->userdata->tel or null }} / {{ $visiter->agenda->user->userdata->cel or null }}</p>
        <p><strong>Período: </strong>{{ $visiter->agenda->period->name  or null}}</p>
        <p><strong>Data: </strong>{{ Helper::ConverterBR($visiter->agenda->date_marked, true) }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('layouts.notifications') {{ Form::open([ 'route'=>array('visiters.update', 'agenda_id'=>$visiter->agenda->id) ]) }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('q1','A caravana é nova?') }} {{ Form::radio('q1', 'Sim', in_array($q['q1'], array('Sim'))) }} Sim {{ Form::radio('q1', 'Não', in_array($q['q1'], array('Não'))) }} Não
                </div>
            </div>
        </div>

     

        @foreach($questions as $k => $question)
            @if(($questions->count()+1) >= ($k+2))
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <p><strong>{{ $question->title }}</strong> 
                            {{ Form::text('q'.($k+2), $q['q'.($k+2)], ['class'=>'form-control']) }}
                           
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
      
        <div class="row">
            <div class="col-md-2">
                <p><i class="fa fa-arrow-left"></i><a href="javaScript:history.back(-1);" class="link">Voltar</a></p>
            </div>
            <div class="col-md-4 text-right">
                {{ Form::submit('Salvar Edição',['class'=>'btn btn-primary']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
