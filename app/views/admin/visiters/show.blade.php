@extends('layouts.admin') 
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro pós visita 
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-6 alert alert-info">
        <h3>Dados do agendamento</h3>
        <p><strong>Instituto: </strong>{{ $visiter->agenda->institute->name }}</p>
        <p><strong>Nº de pessoas: </strong>{{ $visiter->agenda->num_group }}</p>
        <p>
            <strong>Líder responsável: </strong>
            @if(isset($visiter->agenda->user->id))
            {{ $visiter->agenda->user->userdata->username }} - {{ $visiter->agenda->user->userdata->tel }} / {{ $visiter->agenda->user->userdata->cel }}
            @else
            Líder apagada pelo Administrador.
            @endif
        </p>
        <p><strong>Período: </strong>{{ $visiter->agenda->period->name }}</p>
        <p><strong>Data: </strong>{{ Helper::ConverterBR($visiter->agenda->date_marked, true) }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <p><strong>A caravana é nova?</strong> 
                    <p>R.:{{ $q['q1'] }}</p>
                </div>
            </div>
        </div>
        
        @foreach($questions as $k => $question)
             @if(($questions->count()+1) >= ($k+2))
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <p><strong>{{ $question->title }}</strong> 
                            <p>R.:{{ $q['q'.($k+2)] or '--' }}</p>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

        <div class="row">
            <div class="col-md-6 text-right">
               <a href="javaScript:history.back(-1);"><i class="fa fa-arrow-left"></i> Voltar</a>
            </div>
        </div>
        
    </div>
</div>
@stop
