@extends('layouts.admin') 
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro pós visita <small>Criar novo</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-6 alert alert-info">
        <h3>Dados do agendamento</h3>
        <p><strong>Instituto: </strong>{{ $agenda->institute->name or null }}</p>
        <p><strong>Nº de pessoas: </strong>{{ $agenda->num_group }}</p>
        <p><strong>Líder responsável: </strong>{{ $agenda->user->userdata->username or null }} - {{ $agenda->user->userdata->tel or null }} / {{ $agenda->user->userdata->cel or null }}</p>
        <p><strong>Período: </strong>{{ $agenda->period->name or null }}</p>
        <p><strong>Data: </strong>{{ Helper::ConverterBR($agenda->date_marked, true) }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('layouts.notifications') {{ Form::open(['route'=>array('visiters.store', Route::current()->getParameter('slug'), $agenda->id)]) }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('q1','A caravana é nova?') }} {{ Form::radio('q1', 'Sim') }} Sim {{ Form::radio('q1', 'Não') }} Não
                </div>
            </div>
        </div>
      
        {{--
        ANO DA VISITA DA CARAVANA
        mes VISITA DA CARAVANA
        dia
        nome instituto
        CIDADE DE ORIGEM DA CARAVANA
        estado DE ORIGEM DA CARAVANA
        nome lider cpf lider


        CLIENTES REAIS DA CARAVANA
        Quantidade de cliente que a líder levou para o Instituto

        SUPER-RELAXANTE
        Quantas clientes da caravana fizeram o serviço

        CLIENTE FREQUENTE SUPER-RELAXANTE
        Quantidade de clientes que já realizavam o serviço anteriomente

        SR APTOS DE 1ª VEZ
        Quantas cliente de 1ª vez foram aptas para o SR

        SR NÃO APTOS DE 1ª VEZ
        Quantos clientes de 1ª vez não foram aptos a realizar o SR

        CLIENTE FREQUENTE CACHUÁ 
        Quantas crianças realizaram o serviço Cachuá e NÃO são de 1ª vez

        CACHUÁ APTOS DE 1ª VEZ
        Quantas crianças realizaram o serviço de Cachuá pela 1ª vez?
        --}}
        @foreach($questions as $k => $q)
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <p>{{ $q->title }}</p>
                        <p>{{ $q->lead }}</p>
                        {{ Form::text('q'.($k+2), null, ['class'=>'form-control']) }}
                    </div>
                </div>
            </div>
        @endforeach
      
        <div class="row">
            <div class="col-md-6 text-right">
                {{ Form::submit('Cadastrar',['class'=>'btn btn-primary']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop
