@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Editar instituto <small>{{ $institute->name }}</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	
	 @include('layouts.notifications')
			
		{{ Form::open(['route'=>array('admin.institutos.update', $institute->id), 'method'=>'put']) }}
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						{{ Form::checkbox('active', 1, in_array('1', [$institute->active])) }} <span>Ativado</span>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('name','Nome') }}
						{{ Form::text('name', $institute->name, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('slug','URL (não editável, apenas para visualizar*)') }}
						{{ Form::text('slug', url().'/instituto/'.$institute->slug, ['class'=>'form-control', 'disabled']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('tel','Telefone') }}
						{{ Form::text('tel', $institute->tel, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('business_hour','Horário de funcionamento') }}
						{{ Form::text('business_hour', $institute->business_hour, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('cep','CEP') }}
						{{ Form::text('cep', $institute->cep, ['class'=>'form-control']) }}
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('address','Endereço') }}
						{{ Form::text('address', $institute->address, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('reference','Referência') }}
						{{ Form::text('reference', $institute->reference, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('district','Bairro') }}
						{{ Form::text('district', $institute->district, ['class'=>'form-control']) }}
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('state_id','Estado') }}
						{{ Form::select('state_id', [''=>'Selecione estado']+$states, $institute->state_id, ['class'=>'form-control','id'=>'state_id']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('city_id','Cidade') }}
						{{ Form::select('city_id', [''=>'Selecione cidade']+$cities, $institute->city_id, ['class'=>'form-control','id'=>'city_id']) }}
					</div>
				</div>
			</div>
			<div class="row" style="margin-bottom:15px;">
				<div class="col-md-12">
					{{ Form::label('responsaveis', 'Emails GCs (se for mais de 1, coloque os emails separados por vírgula)**O ultimo espaço não deve ter vírgula') }}
					{{ Form::textarea('responsaveis', $resp, ['class'=>'form-control']) }}
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 text-right">
					{{ Form::submit('Editar Instituto',['class'=>'btn btn-primary']) }}
				</div>
			</div>



			
		{{ Form::close() }}
	
</div>
@stop
