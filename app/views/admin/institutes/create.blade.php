@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Institutos <small>Criar novo</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	
	 @include('layouts.notifications')
			
		{{ Form::open(['route'=>'admin.institutos.store']) }}
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('name','Nome') }}
						{{ Form::text('name', null, ['class'=>'form-control']) }}
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('tel','Telefone') }}
						{{ Form::text('tel', null, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('business_hour','Horário de funcionamento') }}
						{{ Form::text('business_hour', null, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('cep','CEP') }}
						{{ Form::text('cep', null, ['class'=>'form-control']) }}
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('address','Endereço') }}
						{{ Form::text('address', null, ['class'=>'form-control']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('reference','Referência') }}
						{{ Form::text('reference', null, ['class'=>'form-control']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('district','Bairro') }}
						{{ Form::text('district', null, ['class'=>'form-control']) }}
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('state_id','Estado') }}
						{{ Form::select('state_id', [''=>'Selecione estado']+$states, null, ['class'=>'form-control','id'=>'state_id']) }}
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						{{ Form::label('city_id','Cidade') }}
						{{ Form::select('city_id', [''=>'Selecione cidade'], null, ['class'=>'form-control','id'=>'city_id']) }}
					</div>
				</div>
			</div>
			<div class="row" style="margin-bottom:15px;">
				<div class="col-md-12">
					{{ Form::label('responsaveis', 'Emails GCs (se for mais de 1, coloque os emails separados por vírgula)**O ultimo espaço não deve ter vírgula') }}
					{{ Form::textarea('responsaveis', null, ['class'=>'form-control']) }}
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12 text-right">
					{{ Form::submit('Criar novo Instituto',['class'=>'btn btn-primary']) }}
				</div>
			</div>



			
		{{ Form::close() }}
	
</div>
@stop
