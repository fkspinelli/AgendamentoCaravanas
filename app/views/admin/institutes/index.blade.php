@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Institutos <small>({{ $institutes->getTotal() }})</small>
        </h1>
    </div>
</div>
<!-- /.row -->
<div class="col-md-12">
	<div class="alert alert-info text-right">
		<a href="{{ route('admin.institutos.create') }}" class="btn btn-primary "><i class="fa fa-plus"></i> NOVO</a>
	</div>
	 @include('layouts.notifications')
	@if($institutes->count() > 0)
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Nome</th>
				<th>Tel</th>
				<th>Cidade</th>
				<th>GCs</th>
				<th>Ativo</th>
				<th>Editar</th>
				<th>Apagar</th>
				
			</tr>
			@foreach($institutes as $i)
				<tr>
					
					<td>{{ $i->name }}</td>
					<td>{{ $i->tel }}</td>
					<td>{{ $i->city->name or null }} - {{ $i->city->state->uf or null }}</td>
					<td>
						@foreach($i->managers as $m)
							<p>- {{ $m->email }} @if(isset($m->user->id)) <span style="display:inline-block;" class="alert alert-success">confirmado</span> @endif</p>
						@endforeach
					</td>
					<td>
						{{ $i->active == '1' ? '<i class="text-success fa fa-check"></i>' : '<i class="text-danger fa fa-close"></i>' }}
					</td>
					
					
					<td><a href="{{ route('admin.institutos.edit', ['id'=>$i->id]) }}"><i class="fa fa-edit"></i></a></td>
					
					<td>
						{{ Form::open(['route'=>array('admin.institutos.destroy', 'id'=>$i->id), 'method'=>'delete']) }}
						<button type="submit">
			                <i class="fa fa-trash"></i>
			            </button>
						
						{{ Form::close() }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ $institutes->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos conteúdo para esta área</p>
	</div>
	@endif
</div>
@stop
