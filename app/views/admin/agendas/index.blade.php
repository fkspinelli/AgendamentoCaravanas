@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Agendas <small>({{ $agendas->getTotal() }})</small>
        </h1>
        {{--<p><a href="{{ route('admin.agendamento.create') }}"><i class="fa fa-calendar"></i> Gerar manualmente nova agendamento</a></p>--}}
    </div>
</div>
<!-- /.row -->
<div class="row" id="searchBlock">
	<div class="col-md-10">
		{{ Form::open(['route'=>$search_action, 'method'=>'get', 'class'=>'form-inline']) }}
		Filtrar por:
		@if(Sentry::getUser()->hasAccess('admin')) 
			{{ Form::select('institute_id', [''=>'Selecione Institutos']+$institutes, Input::get('institute_id'), ['class'=>'form-control']) }}
		@endif
		{{ Form::select('status', [''=>'Status']+$status, Input::get('status'), ['class'=>'form-control']) }}
		{{ Form::text('from', Input::get('from'), ['id'=>'from','class'=>'form-control','placeholder'=>'Data início']) }}
		{{ Form::text('to', Input::get('to'), ['id'=>'to','class'=>'form-control','placeholder'=>'Data término']) }}
		{{ Form::button('<i class="fa fa-search"></i> Buscar', ['class'=>'btn btn-primary','type'=>'submit']) }}
		{{ Form::close() }}
	</div>
	<div class="col-md-2 text-right">
		{{ Form::open(['route'=>$excel_action]) }}
		@foreach($agendas_all as $a)
			{{ Form::hidden('agendas[]', $a->id) }}
		@endforeach
		{{ Form::button('<i class="fa fa-file-excel-o"></i> Gerar relatório', ['class'=>'btn btn-success','type'=>'submit']) }}
		{{ Form::close() }}
	</div>
</div>
<div class="row" style="margin-bottom:20px;">
	<div class="col-md-6 pull-right">
		<div class="form-inline text-right">
		{{ Form::label('actionAgenda', 'Com os marcados:', ['style'=>'color:#333;']) }}
		{{ Form::select('actionAgenda', [''=>'Selecione ação','1'=>'Confirmar agendamento','2'=>'Cancelar agendamento','0'=>'Deixar em pendência','3'=>'Marcar como realizado'], null, ['class'=>'form-control']) }}
		{{ Form::button('<i class="fa fa-arrow-right"></i> Realizar ação', ['class'=>'btn btn-primary', 'id'=>'btn_action_agenda']) }}
		</div>
	</div>
	{{-- Modal para Justificativa de cancelamento --}}
	<!-- Modal -->
		<div id="modal_justify" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Justificativa e sugestão de nova data para agendamento</h4>
		      </div>
		      <div class="modal-body">
		        <div class="row">
		        	<div class="col-md-12">
		        		<p><strong>Explique aqui para o líder da caravana e fique a vontade para sugerir nova data para agendamento</strong></p>
		        		<p>* Esta justificativa será enviada por email ao líder responsável pelo agendamento.</p>
		        		<div class="row">
		        			<div class="col-md-12">{{ Form::textarea('justify_text', null, ['class'=>'form-control', 'id'=>'justify_text']) }}</div>
		        		</div>
		        		<div class="row">
		        			<div class="col-md-12">{{ Form::button('<i class="fa fa-arrow-right"></i> Enviar sugestão e justificativa', ['style'=>'margin-top:15px;','class'=>'btn btn-primary pull-right', 'id'=>'btn_action_justify']) }}</div>
		        		</div>
		        		
		        	</div>
		        </div>
		      </div>
		    </div>

		  </div>
		</div>
</div>
<div class="col-md-12">
	
	@include('layouts.notifications')
	
	@if($agendas->count() > 0)
    <div class="table-responsive">
    	{{ Form::open(['route'=>$status_action, 'id'=>'frmAgendaAction']) }}
    	{{ Form::hidden('action') }}
    	{{ Form::hidden('justify') }}
        <table class="table table-bordered table-hover table-striped">
			<tr>
				<th>Instituto</th>
				<th>Transferir</th>
				<th>Nº de pessoas</th>
				<th>Líder responsável</th>
				<th>Período</th>
				<th>Status</th>
				<th>{{ Form::checkbox('selec_all') }} Todos</th>
			</tr>
			@foreach($agendas as $agenda)
				<tr>
					<td>{{ $agenda->institute->name or "Não" }}</td>
					<td>
						@if($agenda->status == '0')
							<a data-toggle="modal" data-target="#modal_ag_{{ $agenda->id }}" class="btn btn-info" href=""><i class="fa fa-share"></i></a>
							  
						@else
							<span>--</span>
						@endif
					</td>
					<td>{{ $agenda->num_group }}</td>
					<td>
						@if(isset($agenda->user->id))
							<p>{{ $agenda->user->userdata->username }}</p>
							<p>{{ $agenda->user->email }}</p>
							<p> {{ $agenda->user->userdata->city->name }} - {{ $agenda->user->userdata->city->state->uf }} </p>
							<p>{{ $agenda->user->userdata->tel }} / {{ $agenda->user->userdata->cel }}</p>
						 @else
                        	<p>Líder apagado pelo administrador.</p>
                        @endif
					</td>
					<td>{{ Helper::ConverterBR($agenda->date_marked, true) }}<br/>{{ $agenda->period->name }}</td>
					
					<td>{{ $agenda->statusText() }}
						@if(isset($agenda->visiter->id))
							<span style="margin:10px 0; display:block;" class="alert alert-info">Pós visita realizado</span>
							<p><a href="{{route($route_show_visiters,['id'=>$agenda->visiter->id])}}">Visualizar pós visita <i class="fa fa-arrow-right"></i></a></p>
							@if(Route::current()->getParameter('slug') != null)
							<p><a href="{{route('visiters.edit',['id'=>$agenda->visiter->id])}}">Editar pós visita <i class="fa fa-arrow-right"></i></a></p>
							@endif
						@endif
					</td>
					<td>
						{{ Form::checkbox('agenda_ids[]', $agenda->id, false, ['class'=>'agenda_ids']) }}
					</td>
				</tr>
			@endforeach
		</table>
		{{ Form::close() }}

		@foreach($agendas as $agenda)

			<!-- Modal -->
		                        <div id="modal_ag_{{ $agenda->id }}" class="modal fade" role="dialog">
		                          <div class="modal-dialog">

		                            <!-- Modal content-->
		                            <div class="modal-content">
		                              <div class="modal-header">
		                                <button type="button" class="close" data-dismiss="modal">&times;</button>
		                                <h4 class="modal-title">Deseja transferir este agendamento?</h4>
		                              </div>
		                              <div class="modal-body">
		                               
		                                 	<div class="row">
		                                 		<div class="col-md-12">
		                                 			@if(isset($agenda->user->id))
		                                 			<p><strong>Responsável pelo agendamento:</strong> {{ $agenda->user->userdata->username }}</p>

				                                    <p><strong>Email:</strong> {{ $agenda->user->email }}</p>
				                                    <p><strong>Tel:</strong> {{ $agenda->user->userdata->tel }} {{ $agenda->user->userdata->cel }}</p>
				                                    @else
				                                    	<p>Líder desabilitado do sistema.</p>
				                                    @endif
				                                   
		                                 		</div>
		                                 	</div>
											{{ Form::open(['route'=>array($route_transfer, 'slug'=>Route::current()->getParameter('slug'),'agenda'=>$agenda->id)]) }}
			                                 	<div class="row">
			                                 		<div class="col-md-12">
				                                 		<div class="form-group">
				                                 			{{ Form::select('institute_id', [''=>'Selecione Instituto']+$institutes, $agenda->institute->id, ['class'=>'form-control']) }}
				                                 		</div>
			                                 		</div>
			                                 	</div>
			                                   
			                                    <div class="row">
			                                    	<div class="col-md-8">
			                                    		<p><small>* Lembrando que esta ação enviará um email ao GC responsável pelo instituto a ser transferido, podendo ele aceitar ou não sua transferência.</small></p>
			                                    	</div>
			                                    	<div class="col-md-4 text-right">
			                                    		{{ Form::button('Sim',['class'=>'btn btn-success', 'type'=>'submit']) }}
			                                    		<button type="button" class="btn btn-danger" data-dismiss="modal">Não</button>
			                                    	</div>
			                                    </div>
		                                    {{ Form::close() }}
		                                 
		                                 	
		                               
		                              </div>
		                              <div class="modal-footer">
		                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
		                              </div>
		                            </div>

		                          </div>
		                        </div>
		@endforeach
		{{ $agendas->appends($_GET)->links() }}
	</div>
	@else
	<div class="alert alert-warning">
		<p><i class="fa fa-marker"></i> Ainda não temos agendamentos</p>
	</div>
	@endif
</div>
@stop
