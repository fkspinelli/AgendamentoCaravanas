@extends('layouts.admin')
@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Agende uma caravana
        </h1>
       
    </div>
</div>
<!-- /.row -->
<div class="row" id="searchBlock">
	<div class="col-md-10">
		{{ Form::open(['route'=>array('admin.agendamento.create', Route::current()->getParameter('slug')), 'method'=>'get']) }}
			{{ Form::text('cpf', Input::get('cpf'), ['placeholder'=>'Digite CPF de uma líder']) }}
			{{ Form::submit('Buscar') }}
		{{ Form::close() }}
	</div>
	<div class="col-md-2 text-right">
	
	</div>
</div>
<div class="row">
	<div class="col-md-12">	 
		@include('layouts.notifications')
		@if($user != null)
		    {{ Form::open(['route'=>'agendamento.store']) }}
		    {{ Form::hidden('admin',1) }}
		    {{ Form::hidden('user_id_admin',$user->id) }}
		    {{ Form::hidden('slug',Route::current()->getParameter('slug')) }}
		    {{ Form::hidden('cpf', Input::get('cpf')) }}
		    {{ Form::hidden('institute_id', $institute->id) }}
			<table class="table table-bordered">
				<tr>
					<td>Nome</td>
					<td>{{ $user->userdata->username }}</td>
				</tr>
				<tr>
					<td>Data</td>
					<td>{{ Form::text('date_marked', null, ['class'=>'form-control']) }}</td>
				</tr>
				<tr>
					<td>Período</td>
					<td>
						<select name="period_id" class="form-control">
							<option value="" selected disabled>Selecione o período do dia</option>
							<option value="1">Manhã</option>
							<option value="2">Tarde</option>
							<option value="3">Noite</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>N pessoas</td>
					<td><input type="number" name="num_group" min="14" class="form-control" placeholder="Digite a quantidade de pessoas*"></td>
				</tr>
			
				<tr>
					<td colspan="2">{{ Form::button('Enviar', ['class'=>'btn btn-primary', 'type'=>'submit']) }}</td>
				</tr>
			</table>
			{{ Form::close() }}
		@endif
	</div>
</div>
@stop
