@section('content')
	<!-- <img id="desenho" src="../img/caravanas-img.png"> -->
	<div class="desenho"></div>
	<section>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<h4 class="resposta-recuperar-senha">Olá belezete,<br><br>Você está no site Caravana da Beleza!</h4><br>
			</div>
		</div>
		<div class="row">
			<div class="box-cadastro resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Entre no seu Perfil</h3>
			 	</div>
			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
				  	{{ Form::open(['route'=>'user.post.login']) }}
				  		@include('layouts.notifications')
						<div class="form-group">
							<input type="text" name="cpf" id="cpf" value="{{ Input::old('cpf') }}" class="form-control"  placeholder="Digite seu CPF">
						</div>
						<div class="form-group">
							<input type="text" name="birthday" id="birthday" class="form-control"  placeholder="Digite sua data de nascimento">
						</div>
					  	{{--<p class="col-lg-10 col-md-9 col-sm-10 col-xs-12" style="margin-left:-15px; margin-right:15px;">Esqueceu sua senha ou seu login? <a href="{{ route('password.remind') }}" class="laranja"><u>Clique aqui.</u></a></p>--}}
				 		<button type="submit" class=" truncate btn btn-danger col-lg-2 col-md-3 col-sm-2 col-xs-12" style="font-size: 16px;">Entrar</button>
				  	{{ Form::close() }}
				  	
			 	</div>
			</div>
		</div>
	
	</section>
@stop