@section('content')
<div class="desenho-2"></div>
<section>
    @include('layouts.partials.user.common.menu')
    <div class="row box">
        <!-- <h2 class="roxo editar-perfil">Editar perfil</h2> -->
        <div class="box-cadastro lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="box-cadastro-header">
                <!-- <h3>{{ $text_self }}</h3> -->
                <h3>Editar Perfil</h3>
            </div>
            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @include('layouts.notifications')

                {{ Form::open(['route'=> array('user.front.update', $user->id), 'method'=>'put', 'files'=>true ]) }}
                <fieldset>
                    <legend class="roxo">1. Meus dados pessoais:</legend>
                    <div class="row">
                        <!-- <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
					  				<div class="row"> -->
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                            <label for="fileUpload">
                                <div id="image-holder" style="width:120px;height:120px;background-image:url({{ asset($user->getImage(true)) }});background-size:cover;"></div>
                            </label>
                            <input id="fileUpload" name="src" type="file" />
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="username" value="{{ $user->userdata->username }}" class="form-control" placeholder="Nome">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="email" value="{{ $user->email }}" class="form-control" placeholder="E-mail">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="cpf" value="{{ $user->userdata->cpf }}" class="form-control" placeholder="CPF">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{ Helper::ConverterBR($user->userdata->birthday,true) }}"  name="birthday" class="form-control" placeholder="Data de Aniversário">
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="fb_profile" value="{{ $user->userdata->fb_profile or null }}" class="form-control" placeholder="Perfil do facebook">
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="cep" value="{{ $user->userdata->cep}}" class="form-control" placeholder="CEP">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{ $user->userdata->address}}" name="address" class="form-control" placeholder="Endereço">
                                </div>
                            </div>
                            
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {{ Form::select('state_id', [''=>'Selecione estado']+$states, $user->userdata->city->state->id, ['class'=>'form-control', 'id'=>'state_id']) }}
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                {{ Form::select('city_id', [''=>'Selecione cidade']+$cities, $user->userdata->city->id, ['class'=>'form-control', 'id'=>'city_id']) }}
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="tel" value="{{ $user->userdata->tel }}" class="form-control" placeholder="Telefone">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="cel" value="{{ $user->userdata->cel }}" class="form-control" placeholder="Celular">
                            </div>
                            <div class="form-group  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {{ Form::select('profession_id', [''=>'Selecione estado']+$professions, $user->userdata->profession_id, ['class'=>'form-control']) }}
                            </div>
                            <div id="cab"  class="col-md-12">
                                <span>Sou cliente do Clube Amiga da Beleza?</span>
                                <label class="radio-inline">
                                    {{ Form::radio('cab', 1, in_array(1, [$user->userdata->cab])) }}Sim
                                </label>
                                <label class="radio-inline">
                                    {{ Form::radio('cab', 0, in_array(0, [$user->userdata->cab])) }}Não
                                </label>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" name="cab_num" class="form-control" value="{{$user->userdata->cab_num or Input::old('cab_num') }}" placeholder="Número de inscrição do CAB" @if(!isset($user->userdata->cab_num) and !Input::old('cab_num')) style="display:none;" @endif>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ $block_institutes }}
                    <div class="row">
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @if(Sentry::getUser()->hasAccess('colaborator'))
                            <button data-action="nao"  type="button" class="cancelar-lider truncate btn col-lg-4 col-md-4 col-sm-4 col-xs-12" style="font-size: 10px;">NÃO QUERO MAIS SER LIDER</button>
                            @else
                            <button data-action="sim" type="button" class="cancelar-lider truncate btn col-lg-4 col-md-4 col-sm-4 col-xs-12" style="font-size: 10px;">QUERO SER LIDER</button>
                            @endif
                            <button type="submit" class="truncate btn btn-danger col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="font-size: 16px;">Salvar</button>
                        </div>
                    </div>
                </fieldset>
                {{ Form::close() }}

                <hr/>

                {{--
                {{ Form::open(['route'=> array('user.update.password', $user->id), 'method'=>'put' ]) }}
                <fieldset>
                    <legend class="roxo">Alterar senha:</legend>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <input type="password" class="form-control" name="password" placeholder="Digite sua nova senha">
                        </div>
                        <div class="form-group col-md-6">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirme sua nova senha">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="truncate btn btn-danger" style="font-size: 16px; padding:10px 5%">Alterar</button>
                        </div>
                    </div>
                </fieldset>
                {{ Form::close() }}
                --}}

            </div>
        </div>
    </div>
</section>
@stop
