@section('content')
<div class="desenho-2"></div>
<section>
    @include('layouts.partials.user.common.menu')
    <div class="row box">
        <!-- <h2 class="roxo editar-perfil">Listagem de líderes</h2> -->
        <div class="box-cadastro lider list-lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="box-cadastro-header">
                <h3>Veja as caravanas que encontramos para você</h3>
            </div>
            @include('layouts.notifications')
            <div class="content-list">
                @forelse($leaders->chunk(2) as $chunk)
                <div class="row">
                	@foreach($chunk as $user)
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-list-lider">
                        <div class="content">
                            <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">{{ $user->getImage() }}</div>
                            <div class="list-lider-detalhes col-lg-8 col-md-7 col-sm-12 col-xs-12">
                                <h4>{{ $user->userdata->username }}</h4>
                                <p><b>E-mail:</b></p>
                                <a href="mailto:{{ $user->email }}" target="_top"><p>{{ $user->email }}</p></a>
                                <p><b>Telefone(s):</b></p>
                                <p>{{ $user->userdata->tel }} / {{ $user->userdata->cel }}</p>
                                @if($user->userdata->fb_profile != '')
                                <p><i class="fa fa-facebook-official"></i> / {{ $user->userdata->fb_profile }}</p>
                                @endif
                                <p><b>Leva Caravanas para os Institutos:</b></p>
                                @foreach($user->institutes as $i)
                                <p>{{ $i->name }}</p>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @empty
                <div class="row">
                    <div style="padding-top:20px;" class="col-md-12">
                        <p>Olá Belezete! Ainda não encontramos caravanas para você, mas não desanime. Você ainda pode ser líder de caravanas.</p>
                        <p>Que tal experimentar?</p><br/>
                        <p><button data-action="sim" style="text-transform:uppercase;" type="button" class="cancelar-lider truncate btn col-lg-4 col-md-4 col-sm-4 col-xs-12" style="font-size: 10px;">Quero me tornar uma líder</button></p>
                    </div>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</section>
@stop

