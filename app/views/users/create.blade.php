@section('content')
		
		{{ $introduction }}

		<div class="row box">
			<div class="box-cadastro lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="bg-roxo lider box-cadastro-header">
			 		<h3>Faça seu cadastro</h3>
			 	</div>
			 	@include('layouts.notifications')
			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">

				  	{{ Form::open(['route'=>'users.store', 'files'=>true]) }}

				  	{{ Form::hidden('usergroup', Route::current()->getName()) }}
				  		<fieldset>
				  			<legend class="roxo">1. Preencha seus dados pessoais:</legend>
				  			<div class="row">
					  			
					  		
				  				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
				  					<label for="fileUpload">
				  						<div id="image-holder" style="width:120px;height:120px;background-image:url({{ asset('img/avatar.jpg') }});background-size:cover;"></div>
				  					</label>
				  					<input id="fileUpload" name="src" type="file" />
				  				</div>
					  			
					  			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
					  				<div class="row">
					  					<div class="col-md-12">
					  						<div class="form-group">
					  							<input type="text" name="username" value="{{ Input::old('username') }}" class="form-control" placeholder="Nome Completo">
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" value="{{ Input::old('email') }}" name="email" class="form-control" placeholder="E-mail">
					  						</div>
					  					</div>
					  				
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" name="cpf" value="{{ Input::old('cpf') }}" class="form-control" placeholder="CPF">
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" value="{{ Input::old('birthday') }}"  name="birthday" class="form-control" placeholder="Data de Aniversário">
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" name="fb_profile" value="{{ Input::old('fb_profile') }}" class="form-control" placeholder="Perfil do facebook (opcional)">
					  						</div>
					  					</div>

					  					<div class="col-md-4">
					  						<div class="form-group">
					  							<input type="text" name="cep" value="{{ Input::old('cep') }}" class="form-control" placeholder="CEP">
					  						</div>
					  					</div>

					  					<div class="col-md-2">
					  						<a href="http://www.buscacep.correios.com.br" target="_blank" class="link-black size12" style="line-height: 48px;">Buscar CEP</a>
					  					</div>

					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" value="{{ Input::old('address') }}" name="address" class="form-control" placeholder="Endereço">
					  						</div>
					  					</div>
					  				
					  					

					  					<div class="col-md-6">
					  						<div class="form-group">
					  							{{ Form::select('state_id', [''=>'Selecione estado']+$states, null, ['class'=>'form-control', 'id'=>'state_id']) }}
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							{{ Form::select('city_id', [''=>'Selecione cidade']+$cities, null, ['class'=>'form-control', 'id'=>'city_id']) }}
					  						</div>
					  					</div>

					  					

					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" name="tel" value="{{ Input::old('tel') }}" class="form-control"  placeholder="Telefone">
					  						</div>
					  					</div>
					  					<div class="col-md-6">
					  						<div class="form-group">
					  							<input type="text" name="cel" value="{{ Input::old('cel') }}" class="form-control"  placeholder="Celular">
					  						</div>
					  					</div>
					  					<div class="col-md-12">
					  						<div class="form-group">
					  							{{ Form::select('profession_id', [''=>'Selecione profissão']+$professions, null, ['class'=>'form-control']) }}
					  						</div>
					  					</div>
					  					<div id="cab" class="col-md-12">
					  						<span>Sou cliente do Clube Amiga da Beleza?</span>
											<label class="radio-inline">
										      {{ Form::radio('cab', 1) }}Sim
										    </label>
										    <label class="radio-inline">
										      {{ Form::radio('cab', 0) }}Não
										    </label>
					  					</div>
					  					<div class="col-md-12">
					  						<div class="form-group">
					  							<input type="text" name="cab_num" value="{{ Input::old('cab_num') }}" class="form-control"  placeholder="Número de inscrição do CAB" @if(!Input::old('cab_num')) style="display:none; @endif">
					  						</div>
					  					</div>
					  				</div>
					  			</div>
					 		</div>

							
				  		</fieldset>
						
				  			<div>	
								{{ $loop_institutes or null }}
								<hr>

						 		<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
						 			<div class="checkbox col-lg-10 col-md-10 col-sm-12 col-xs-12">
									  	<label>
											{{ Form:: checkbox('terms',1) }}
									  		</label>
									  	<a class="aceitar-term" data-toggle="modal" data-target="#termosModal">Li e aceito os termos e condições.</a>
									</div>
									<button type="submit" class="col-lg-2 col-md-2 col-sm-12 col-xs-12 btn btn-danger">Enviar</button>
						 		</div>

					 		</div>

							
				  		</fieldset>
				  	{{ Form::close() }}
			 	</div>
			</div>
		</div>
	
	</section>
	
	@include('layouts.partials.user.common.terms')


@stop