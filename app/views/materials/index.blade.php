@extends('layouts.base')

@section('content')
	<div class="desenho-2"></div>
	<section>

		@include('layouts.partials.user.common.menu')

		<div class="menu-perfil">
			<div class="row ola-usuario col-lg-7 col-md-12 col-sm-12 col-xs-12">
				{{-- <h2 class="roxo st_ryde_italicitalic size45">Material de Apoio</h2> --}}
			</div>


		</div>
		<div class="row box">
			<div class="box-cadastro lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Material de Apoio</h3>
			 	</div>
			 	<div style="padding: 25px;">
				  	<div class="row">
				  		<div class="col-sm-12">
				  			{{--<h4 class="roxo size25" style="margin-bottom: 20px;">Aproveite esses conteúdos para aprender ainda mais e ficar por dentro de tudo que o Beleza Natural tem pra você. Aproveite!</h4>--}}
				  			<p class="size20 cl-525252" style="margin-bottom: 20px;">Este é um espaço para você, Líder, estar sempre antenada com as novidades do Beleza Natural. Aqui você encontra materiais que poderá utilizar para aprender mais sobre os nossos produtos e serviços e também para divulgar sua caravana.
				  			<br>
				  			Aproveite!
				  			</p>
				  			<h4 class="roxo size25" style="margin-bottom: 20px;">Peças para Download</h4>
				  		</div>
				  	</div>

			  		<ul class="materials">
				  		@foreach($materials as $m)
			  			<li>
			  				<div class="clearfix">
				  				<div class="icon">
				  					<div class="pull-left" style="padding-right: 20px;">
				  						<img src="{{asset('img')}}/icon-{{explode('.', $m->src)[1]}}.png">
				  					</div>
				  					<div class="pull-left">
				  						<h5><b>{{$m->name}}</b> <i>{{ Carbon\Carbon::parse($m->created_at)->format('d.m.y') }}</i></h5>
				  						<p>{{$m->description}}</p>
				  					</div>
				  				</div>
				  				<div class="button">
				  					<div class="pull-right">
				  						<a href="/uploads/material/{{ $m->src }}" class="truncate btn btn-danger" download="{{ $m->name }}" style="font-size: 24px;padding: 6px 17px;"><img src="{{asset('img/icon-download.png')}}" style="vertical-align: baseline;"> Baixar arquivo</a>
				  					</div>
				  				</div>
			  				</div>
			  			</li>
						@endforeach
{{-- 			  			<li>
			  				<div class="clearfix">
				  				<div class="icon">
				  					<div class="pull-left" style="padding-right: 20px;">
				  						<img src="{{asset('img/icon-zip.png')}}">
				  					</div>
				  					<div class="pull-left">
				  						<h5><b>Nome do material</b> <i>12.10.17</i></h5>
				  						<p>Breve descrição do material loren ipsum</p>
				  					</div>
				  				</div>
				  				<div class="button">
				  					<div class="pull-right">
				  						<a href="#" class="truncate btn btn-danger" download style="font-size: 24px;padding: 6px 17px;"><img src="{{asset('img/icon-download.png')}}" style="vertical-align: baseline;"> Baixar arquivo</a>
				  					</div>
				  				</div>
			  				</div>
			  			</li>
			  			<li>
			  				<div class="clearfix">
				  				<div class="icon">
				  					<div class="pull-left" style="padding-right: 20px;">
				  						<img src="{{asset('img/icon-jpg.png')}}">
				  					</div>
				  					<div class="pull-left">
				  						<h5><b>Nome do material</b> <i>12.10.17</i></h5>
				  						<p>Breve descrição do material loren ipsum</p>
				  					</div>
				  				</div>
				  				<div class="button">
				  					<div class="pull-right">
				  						<a href="#" class="truncate btn btn-danger" download style="font-size: 24px;padding: 6px 17px;"><img src="{{asset('img/icon-download.png')}}" style="vertical-align: baseline;"> Baixar arquivo</a>
				  					</div>
				  				</div>
			  				</div>
			  			</li>
			  			<li>
			  				<div class="clearfix">
				  				<div class="icon">
				  					<div class="pull-left" style="padding-right: 20px;">
				  						<img src="{{asset('img/icon-zip.png')}}">
				  					</div>
				  					<div class="pull-left">
				  						<h5><b>Nome do material</b> <i>12.10.17</i></h5>
				  						<p>Breve descrição do material loren ipsum</p>
				  					</div>
				  				</div>
				  				<div class="button">
				  					<div class="pull-right">
				  						<a href="#" class="truncate btn btn-danger" download style="font-size: 24px;padding: 6px 17px;"><img src="{{asset('img/icon-download.png')}}" style="vertical-align: baseline;"> Baixar arquivo</a>
				  					</div>
				  				</div>
			  				</div>
			  			</li>
			  			<li>
			  				<div class="clearfix">
				  				<div class="icon">
				  					<div class="pull-left" style="padding-right: 20px;">
				  						<img src="{{asset('img/icon-pdf.png')}}">
				  					</div>
				  					<div class="pull-left">
				  						<h5><b>Nome do material</b> <i>12.10.17</i></h5>
				  						<p>Breve descrição do material loren ipsum</p>
				  					</div>
				  				</div>
				  				<div class="button">
				  					<div class="pull-right">
				  						<a href="#" class="truncate btn btn-danger" download style="font-size: 24px;padding: 6px 17px;"><img src="{{asset('img/icon-download.png')}}" style="vertical-align: baseline;"> Baixar arquivo</a>
				  					</div>
				  				</div>
			  				</div>
			  			</li> --}}
			  		</ul>

			 	</div>
			</div>
		</div>
	
	</section>
@stop