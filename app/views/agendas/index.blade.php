@section('content')
<div class="desenho-2"></div>
	<section>
	
	@include('layouts.partials.user.common.menu')
		<div class="row">
			<div class="col-md-6">
				<p class="atencao">
					<b>ATENÇÃO!</b>  O prazo para confirmação dos seus
					agendamento é de 48hs.

					Caso você precise cancelar um agendamento, pedimos
					que entre em contato diretamente com o instituto.
				</p>
			</div>
		</div>	
		<!-- <h2 class="roxo editar-perfil">Caravanas Agendadas</h2> -->
		<ul class="list-tab" role="tablist">
			<li class="bg-rosa3"><a role="tab" data-toggle="tab" href="#agendas_canceladas">Canceladas</a></li>
			<li class="bg-rosa2"><a role="tab" data-toggle="tab" href="#agendas_realizadas">Realizadas</a></li>
			<li class="bg-roxo3"><a role="tab" data-toggle="tab" href="#agendas_aguardando">Aguardando</a></li>
			<li class="bg-roxo2 active"><a role="tab" data-toggle="tab" href="#agendas_confirmadas">Confirmadas</a></li>
		</ul>
	 	<div class="tab-content">
	 
			<div role="tabpanel" class="tab-pane active" id="agendas_confirmadas">
				<div class="row box">
					<div class="box-cadastro lider list-lider caravanas-agendadas resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
					 	<br><br>
					 	<div class="content-list">
					 		@include('layouts.notifications')
						 	@forelse($agendas_confirmadas as $agenda)
							  	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-list-lider">
							  		<div class="content">
							  			<div class="list-lider-detalhes col-md-12">
							  				<h4 class="vermelho">{{ Helper::ConverterBR($agenda->date_marked, true) }} - {{ Helper::DiaSemana($agenda->date_marked) }}</h4>
							  				<p class="azul size12"><b>Período agendado: </b>{{ $agenda->period->name }}</p>
							  				<p class="azul size12"><b>Quantidade de pessoas: </b>{{ $agenda->num_group }} pessoas</p>
							  				<h4>{{ $agenda->institute->name }}</h4>
							  				<p>{{ $agenda->institute->address }}</p>
							  				<p>{{ $agenda->institute->district }} - {{ $agenda->institute->city->name }} - {{ $agenda->institute->state->uf }}  {{ $agenda->institute->reference or null }}</p>
							  				<p>CEP: {{ $agenda->institute->cep }}</p>
							  				<p><i class="fa fa-phone icon-radios bg-laranja2"></i> {{ $agenda->institute->tel }}</p>
							  				<p><i class="fa fa-clock-o icon-radios bg-vermelho"></i> {{ $agenda->institute->business_hour }}.</p>
							  			</div>
							  		</div>
							  	</div>
							 @empty
							 <div class="row">
							 	<div class="col-md-12">Você ainda não possui caravanas agendadas confirmadas.</div>
							 </div>
						  	@endforelse
					 	</div>
					</div>
				</div>
			</div>

					<div role="tabpanel" class="tab-pane" id="agendas_aguardando">
				<div class="row box">
					<div class="box-cadastro lider list-lider caravanas-agendadas resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">

					 	<br><br>
					 	<div class="content-list">
					 		@include('layouts.notifications')
						 	@forelse($agendas as $agenda)
							  	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-list-lider">
							  		<div class="content">
							  			<div class="list-lider-detalhes col-md-12">
							  				<h4 class="vermelho">{{ Helper::ConverterBR($agenda->date_marked, true) }} - {{ Helper::DiaSemana($agenda->date_marked) }}</h4>
							  				<p class="azul size12"><b>Período agendado: </b>{{ $agenda->period->name }}</p>
							  				<p class="azul size12"><b>Quantidade de pessoas: </b>{{ $agenda->num_group }} pessoas</p>
							  				<h4>{{ $agenda->institute->name }}</h4>
							  				<p>{{ $agenda->institute->address }}</p>
							  				<p>{{ $agenda->institute->district }} - {{ $agenda->institute->city->name }} - {{ $agenda->institute->state->uf }}  {{ $agenda->institute->reference or null }}</p>
							  				<p>CEP: {{ $agenda->institute->cep }}</p>
							  				<p><i class="fa fa-phone icon-radios bg-laranja2"></i> {{ $agenda->institute->tel }}</p>
							  				<p><i class="fa fa-clock-o icon-radios bg-vermelho"></i> {{ $agenda->institute->business_hour }}.</p>
							  			</div>
							  		</div>
							  	</div>
							 @empty
							 <div class="row">
							 	<div class="col-md-12">Você ainda não possui caravanas agendadas aguardando.</div>
							 </div>
						  	@endforelse
					 	</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="agendas_realizadas">
				<div class="row box">
					<div class="box-cadastro lider list-lider caravanas-agendadas resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
					 	<br><br>
					 	<div class="content-list">
					 		@include('layouts.notifications')
						 	@forelse($agendas_realizadas as $agenda)
							  	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-list-lider">
							  		<div class="content">
							  			<div class="list-lider-detalhes col-md-12">
							  				<h4 class="vermelho">{{ Helper::ConverterBR($agenda->date_marked, true) }} - {{ Helper::DiaSemana($agenda->date_marked) }}</h4>
							  				<p class="azul size12"><b>Período agendado: </b>{{ $agenda->period->name }}</p>
							  				<p class="azul size12"><b>Quantidade de pessoas: </b>{{ $agenda->num_group }} pessoas</p>
							  				<h4>{{ $agenda->institute->name }}</h4>
							  				<p>{{ $agenda->institute->address }}</p>
							  				<p>{{ $agenda->institute->district }} - {{ $agenda->institute->city->name }} - {{ $agenda->institute->state->uf }}  {{ $agenda->institute->reference or null }}</p>
							  				<p>CEP: {{ $agenda->institute->cep }}</p>
							  				<p><i class="fa fa-phone icon-radios bg-laranja2"></i> {{ $agenda->institute->tel }}</p>
							  				<p><i class="fa fa-clock-o icon-radios bg-vermelho"></i> {{ $agenda->institute->business_hour }}.</p>
							  			</div>
							  		</div>
							  	</div>
							 @empty
							 <div class="row">
							 	<div class="col-md-12">Você ainda não possui caravanas agendadas realizadas.</div>
							 </div>
						  	@endforelse
					 	</div>
					</div>
				</div>
			</div>

			<div role="tabpanel" class="tab-pane" id="agendas_canceladas">
				<div class="row box">
					<div class="box-cadastro lider list-lider caravanas-agendadas resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
					 	<br><br>
					 	<div class="content-list">
					 		@include('layouts.notifications')
						 	@forelse($agendas_canceladas as $agenda)
							  	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-list-lider">
							  		<div class="content">
							  			<div class="list-lider-detalhes col-md-12">
							  				<h4 class="vermelho">{{ Helper::ConverterBR($agenda->date_marked, true) }} - {{ Helper::DiaSemana($agenda->date_marked) }}</h4>
							  				<p class="azul size12"><b>Período agendado: </b>{{ $agenda->period->name }}</p>
							  				<p class="azul size12"><b>Quantidade de pessoas: </b>{{ $agenda->num_group }} pessoas</p>
							  				<h4>{{ $agenda->institute->name }}</h4>
							  				<p>{{ $agenda->institute->address }}</p>
							  				<p>{{ $agenda->institute->district }} - {{ $agenda->institute->city->name }} - {{ $agenda->institute->state->uf }}  {{ $agenda->institute->reference or null }}</p>
							  				<p>CEP: {{ $agenda->institute->cep }}</p>
							  				<p><i class="fa fa-phone icon-radios bg-laranja2"></i> {{ $agenda->institute->tel }}</p>
							  				<p><i class="fa fa-clock-o icon-radios bg-vermelho"></i> {{ $agenda->institute->business_hour }}.</p>
							  			</div>
							  		</div>
							  	</div>
							 @empty
							 <div class="row">
							 	<div class="col-md-12">Você ainda não possui caravanas agendadas canceladas.</div>
							 </div>
						  	@endforelse
					 	</div>
					</div>
				</div>
			</div>
		</div>
	</section>	
@stop