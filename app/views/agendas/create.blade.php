@section('content')
<div class="desenho-2"></div>
	<section>
		
		@include('layouts.partials.user.common.menu')

		<!-- <h2 class="roxo editar-perfil">Agendamento de Caravanas</h2> -->
		<div class="row box">
			<div class="box-cadastro lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Inicie o seu agendamento</h3>
			 	</div>
			 	{{ Form::open(['route'=>'agendamento.store', 'id'=>'frmAgendamento']) }}
			 	<div class="agendamento container col-lg-12 col-md-12 col-sm-12 col-xs-12">
				  @include('layouts.notifications')
				  		<fieldset>
				  			<legend class="roxo">Em qual Instituto gostaria de fazer seu agendamento?</legend>
				  			<div class="row">
								<div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12">
									
									{{ Form::select('institute_id', [''=>'Selecione o Instituto']+$institutes, $institute_id, ['class'=>'form-control']) }}<br/>
									<p>Deseja escolher mais institutos para levar sua caravana? <br/><a href="{{ route('user.front.edit') }}">Clique aqui</a></p>
								</div>
					 		</div>
					 		<div class="row">
					 			{{-- CALENDARIO --}}
								<div id="calendar" style="display:{{ $display_calendar }};" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-calendario">
									<div class="bg-branco content">
										<!-- calendario -->
										<div class="container">
											<p class="ArialNarrow size18">Clique no dia em que deseja agendar sua visita</p>
											<h4 class="size22">Calendário do Instituto:</h4>
											<h4 class="size22 laranja">{{ $institute->name or null }}</h4>
											<div id="datepicker" class="ui-datepicker-bn"></div>
											<p><small>* Você só poderá agendar com até 2 dias de antecedência da data de hoje.</small></p>
										</div>
										<!-- /calendario -->
									</div>
								</div>
								{{-- FIM CALENDARIO --}}
								{{-- DETALHES DO AGENDAMENTO --}}
								<div id="calendar_detail" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 box-calendario bg-cinza">
									<div class="bg-cinza content">
									
										<!-- detalhes do agendamento -->
										<div class="box-cadastro">
										 	<div class="box-cadastro-header bg-vermelho2">
										 		<h3>Detalhes do agendamento</h3>
										 	</div>
										 	
										 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
										 		<h4 class="day_choosen vermelho size22"><span class="data"></span> - <span class="dia"></span></h4>

										 		{{ Form::hidden('date_marked', null, ['class'=>'data']) }}
									
												<div class="form-group">
													<select name="period_id" class="form-control">
														<option value="" selected disabled>Selecione o período do dia</option>
														<option value="1">Manhã</option>
														<option value="2">Tarde</option>
														<option value="3">Noite</option>
													</select>
												</div>
												<div class="form-group">
													<input type="number" name="num_group" min="10" class="form-control" placeholder="Digite a quantidade de pessoas*">
												</div>
											  	<p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ArialNarrow" style="margin-left:-15px; margin-right:15px;"><small>*Quantidade mínima de 10 pessoas</small></p>
										 		<button type="submit" class=" truncate btn btn-danger col-lg-8 col-md-8 col-sm-12 col-xs-12" style="font-size: 16px;">Solicitar Agendamento</button>
										 	</div>
										 	
										</div>
										<!-- /detalhes do agendamento -->

									</div>
								</div>
								{{-- FIM DO DETALHE --}}
					 		</div>
				  		</fieldset>
				 
			 	</div>
			 	{{ Form::close() }}
			</div>
		</div>
	{{--Route::getCurrentRoute()->getParameter('institute_id');--}}
	</section>
	<script>
		jQuery(document).ready(function(){
			@if(Route::getCurrentRoute()->getParameter('institute_id'))
				var pos = $('#calendar').offset().top;
				
				$('html, body').animate({ scrollTop: pos}, 'slow');
			@endif
		});
	</script>
@stop