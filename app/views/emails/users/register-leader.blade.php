<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Agendamento Caravanas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table width="600" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_01.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_02.jpg') }}" style="display:block; border:none;"></td>
						<td width="315" bgcolor="#ED6827"><font face="Arial" size="5" color="#ffffff">Olá Belezete,</font></td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_04.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_05.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_06.jpg') }}" style="display:block; border:none;"></td>
						<td width="517" bgcolor="#F2F2F2">
							<font face="Arial" size="4" color="#6b074f">
								Estamos muito felizes que agora você é uma Líder BN.<br>
								Temos orgulho em espalhar autoestima e cachos e agora você faz parte desse time!
							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_08.jpg') }}" style="display:block; border:none;"></td>
					</tr>
					{{-- <tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_14.jpg') }}" height="100%" width="40px" style="display:block; border:none;"></td>
						<td width="517" bgcolor="#F2F2F2">
							<font face="Arial" size="4" color="#6b074f">
								Mas antes confirme seu email para ativar sua conta, clicando neste link abaixo
								
								<br><br>
								<strong>Conheça os benefícios e as regras:</strong>
							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_15.jpg') }}" height="100%" width="43px" style="display:block; border:none;"></td>
					</tr> --}}
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_09.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#f1f1f1">
					<tr align="center">
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_16.jpg') }}" style="display:block; border:none;" width="88" height="24"></td>
						<td width="421" height="24">
							<a href="{{ route('site.terms') }}" target="_blank">
								<font face="Arial" size="4" color="#65164c">
									RELEIA OS TERMOS E CONDIÇÕES
								</font>
							</a>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_18.jpg') }}" style="display:block; border:none;" width="91" height="24"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_19.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#dddddd">
					<tr align="center">
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_20.jpg') }}" style="display:block; border:none;" width="40" height="110"></td>
						<td width="517" height="110">
							<font face="Arial" size="4" color="#6b074f">
								<strong>Quer ficar por dentro das novidades<br> em primeira mão?</strong><br><br>
								<font color="#34373a">Entre no grupo fechado do Facebook Líderes de Caravana BN!</font>
							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_22.jpg') }}" style="display:block; border:none;" width="43" height="110"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#dddddd">
					<tr align="center">
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_23.jpg') }}" style="display:block; border:none;"></td>
						<td>
							<a href="https://www.facebook.com/groups/1450426205221543/?fref=ts" target="_blank">
								<img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_24.jpg') }}" style="display:block; border:none;">
							</a>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_25.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_26.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_10.jpg') }}" style="display:block; border:none;"></td>
						<td width="521" bgcolor="#F2F2F2">
							<font face="Arial" size="4" color="#6b074f">
								<strong>Seja bem vinda!</strong>
								<br><br>
								Um abraço,<br>
								Equipe Beleza Cacheada
							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_12.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoLider/NewsletterConfirmacaoLider_13.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</body>
</html>