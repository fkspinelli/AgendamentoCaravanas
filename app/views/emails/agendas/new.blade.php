<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Agendamento Caravanas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table width="600" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_01.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_02.jpg') }}" style="display:block; border:none;"></td>
						<td width="254" bgcolor="#ED6827"><font face="Arial" size="5" color="#ffffff">Olá, {{ $m->user->userdata->username }}.</font></td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_04.jpg') }}" style="display:block; border:none;"></td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_05.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_06.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_07.jpg') }}" style="display:block; border:none;"></td>
						<td width="521" bgcolor="#F2F2F2">
							<font face="Arial" size="4" color="#6b074f">
								Um agendamento foi solicitado para o seu instituto. <br>
								Confira os dados deste agendamento. <br><br>

								<strong>Dados do agendamento:</strong>

								<p><strong>Instituto agendado: </strong>{{ $agenda->institute->name }}</p>
								<p><strong>Pessoas: </strong>{{ $agenda->num_group }}</p>
								
								<p><strong>Período</strong>{{ $agenda->period->name }}</p>
								<p><strong>Data: </strong>{{ Helper::ConverterBR($agenda->date_marked, true) }}</p>

							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_09.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_10.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_11.jpg') }}" style="display:block; border:none;"></td>
						<td width="521" bgcolor="#F2F2F2">
							<font face="Arial" size="4" color="#6b074f">
								<strong>Acesse o site para poder confirmar ou cancelar este agendamento. Aguardamos você!</strong><br><br>

								Beijos,<br>
								Equipe Beleza Cacheada
							</font>
						</td>
						<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_13.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/emails/NewsletterConfirmacaoParticipante/NewsletterConfirmacaoParticipante_14.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</body>
</html>