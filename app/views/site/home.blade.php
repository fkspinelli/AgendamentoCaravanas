@section('content')
	<div class="desenho-2"></div>
	<section>
		<div class="menu-perfil">
			<div class="row ola-usuario col-lg-7 col-md-12 col-sm-12 col-xs-12">
				<h2 class="roxo st_ryde_italicitalic size45">Editar perfil</h2>
				<p class="white size18">Ser líder é espalhar autoestima e cachos perfeitos com o Beleza Natural e ainda usufruir de benefícios exclusivos.</p>
			</div>


		</div>
		<div class="row box">
			<div class="box-cadastro lider resposta-redef-senha container col-lg-7 col-md-7 col-sm-12 col-xs-12">
			 	<div class="box-cadastro-header">
			 		<h3>Sou lider</h3>
			 	</div>
			 	<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
				  	<form role="form">
				  		<fieldset>
				  			<legend class="roxo">1. Meus dados pessoais:</legend>
				  			<div class="row">
					  			<img class="foto-usuario col-lg-2 col-md-2 col-sm-12 col-xs-12" src="{{ asset('img/foto-usuario.jpg') }}" alt="Foto Usuário">
						 		
						 		<div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
									<input type="text" class="form-control" id="usr" placeholder="Nome">
								</div>
								<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<input type="text" class="form-control" id="usr" placeholder="E-mail">
								</div>
								<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<input type="text" class="form-control" id="usr" placeholder="Confirme seu E-mail">
								</div>
								<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<input type="text" class="form-control" id="usr" placeholder="CPF">
								</div>
								<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
									<input type="text" class="form-control" id="usr" placeholder="Perfil do facebook">
								</div>
								<div class="aj-form">
									<div class="form-group col-lg-offset-0 col-md-offset-2	col-sm-offset-0 col-xs-offset-0 col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<input type="text" class="form-control" id="usr" placeholder="Senha">
									</div>
									<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<input type="text" class="form-control" id="usr" placeholder="Confirme sua senha">
									</div>
									<div class="form-group col-md-offset-2 col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<select class="form-control">
											<option value="" selected disabled>Selecione estado</option>
											<option value="">Estado 1</option>
											<option value="">Estado 2</option>
											<option value="">Estado 3</option>
										</select>
									</div>
									<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<input type="text" class="form-control" id="usr" placeholder="Cidade">
									</div>
									<div class="form-group col-md-offset-2 col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<input type="text" class="form-control" id="usr" placeholder="Telefone">
									</div>
									<div class="form-group col-lg-5 col-md-5 col-sm-12 col-xs-12">
										<input type="text" class="form-control" id="usr" placeholder="Celular">
									</div>
									<div class="form-group col-lg-offset-2 col-md-offset-2 col-lg-10 col-md-10 col-sm-12 col-xs-12">
										<select class="form-control">
											<option value="" selected disabled>Selecione profissão</option>
											<option value="">Profissão 1</option>
											<option value="">Profissão 2</option>
											<option value="">Profissão 3</option>
										</select>
									</div>
									<div class="col-lg-offset-2 col-md-offset-2 col-lg-5 col-md-6 col-sm-12 col-xs-12"><span>Sou cliente do Clube Amiga da Beleza?</span></div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label class="radio-inline"><input type="radio" name="amigo_beleza">Sim</label></div>
								<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12"><label class="radio-inline"><input type="radio" name="amigo_beleza">Não</label></div>

								<!-- <div><hr class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div> -->


					 		</div>

							
				  		</fieldset>

				  		<legend class="roxo">2. Instituto(s) da minha Caravana:</legend>
				  			<div class="row"> 		
								
								<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
							  		<button type="button" class="btn btn-white " data-toggle="collapse" data-target="#collapse-bahia">Bahia<i class="fa fa-plus fa-right"></i></button>
							  		<div id="collapse-bahia" class="collapse box-collapse">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 1</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 2</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 3</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 4</label></div>
							  		</div>
								</div>

								<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
							  		<button type="button" class="btn btn-white " data-toggle="collapse" data-target="#collapse-espirito-santo">Espírito Santo<i class="fa fa-plus fa-right"></i></button>
							  		<div id="collapse-espirito-santo" class="collapse box-collapse">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 1</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 2</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 3</label></div>
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><label class="checkbox-inline"><input type="checkbox" name="">Opação 4</label></div>
							  		</div>
								</div>
								
								<!-- <div><hr class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div> -->

						 		<div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
						 			<button type="submit" class="cancelar-lider truncate btn col-lg-4 col-md-4 col-sm-4 col-xs-12" style="font-size: 10px;">NÃO QUERO MAIS SER LIDER</button>
				  					<button type="submit" class=" truncate btn btn-danger col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-lg-2 col-md-2 col-sm-2 col-xs-12" style="font-size: 16px;">Salvar</button>
						 		</div>
					 		</div>

							
				  		</fieldset>
				  	</form>
			 	</div>
			</div>
		</div>
	
	</section>
@stop