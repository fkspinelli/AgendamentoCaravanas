<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', ['as'=>'home', 'before'=>'already.log', 'uses'=>'UsersController@login']);
Route::get('termos-e-condicoes', ['as'=>'site.terms', 'uses'=>'SiteController@terms']);

# Usuários
Route::get('cadastro', ['as'=>'users.create', 'before'=>'already.log', 'uses'=>'UsersController@create']);
Route::post('cadastro', ['as'=>'users.store', 'before'=>'already.log', 'uses'=>'UsersController@store']);
Route::post('upload/image', ['as'=>'users.upload.image', 'before'=>'already.log', 'uses'=>'UsersController@uploadImage']);

Route::get('cadastro/participante', ['as'=>'guests.create', 'before'=>'already.log', 'uses'=>'UsersController@create']);



Route::get('cadastro/confirmacao/{activationCode}', ['as'=>'user.register.activation', 'uses'=>'UsersController@activation']);

# Login
Route::get('login', ['as'=>'user.login', 'before'=>'already.log', 'uses'=>'UsersController@login']);
Route::post('login', ['as'=>'user.post.login', 'uses'=>'UsersController@postLogin']);

# Passwords
Route::get('password/reset', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);
Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);
Route::get('password/reset/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);
Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);



# Searches
Route::post('city/search', ['as'=>'city.search', 'uses'=>'CitiesController@searchByState']);


Route::group(['before'=>'auth'], function(){

	#Retono do cadastro com o usário logaodo
	Route::get('cadastro/finalizado', ['as'=>'users.register.return', 'uses'=>'UsersController@registerReturn']);

	Route::get('lideres', ['as'=>'institutes.leaders', 'uses'=>'UsersController@indexApp']);

	# Usuarios
	Route::get('editar', ['as' => 'user.front.edit', 'uses'=>'UsersController@frontEdit']);
	Route::put('editar/{id}', ['as' => 'user.front.update', 'uses'=>'UsersController@frontUpdate']);
	Route::put('editar/pass/{id}', ['as' => 'user.update.password', 'uses'=>'UsersController@updatePass']);
	Route::get('group/edit', ['as' => 'user.update.group', 'uses'=>'UsersController@updateGroup']);


	# Agendamento
	Route::get('agendamento/concluido', ['as'=>'agendamento.confirmed', 'uses'=>'AgendasController@resp']);
	Route::get('agendamento/{institute_id?}', ['as'=>'agendamento.create', 'before'=>'colaborator.access', 'uses'=>'AgendasController@create']);
	Route::post('agendamento', ['as'=>'agendamento.store',  'uses'=>'AgendasController@store']);


	Route::get('agendas', ['as'=>'agendamento.index', 'before'=>'colaborator.access', 'uses'=>'AgendasController@indexApp']);
	
	Route::get('logout', ['as'=>'user.logout', 'uses' => 'UsersController@logout']);

	# Materiais Promocionais
	Route::get('material-de-apoio', ['as'=>'materials.index', 'before'=>'colaborator.access', 'uses'=>'MaterialsController@indexApp']);
});

# Admin Geral
Route::get('admin', ['as'=>'admin.index', 'before'=>'already.log.admin', 'uses'=>'AdminController@index']);
Route::post('admin/login',['as'=>'admin.login', 'uses'=>'AdminController@login']);

# Acao de baixa relatóio
	Route::post('usuarios/download/relatorio', ['as'=>'user.download.excel', 'uses'=>'UsersController@download']);

Route::group(['prefix'=>'admin', 'before'=>'auth|auth.admin'], function(){

	# Institutos
	Route::resource('institutos', 'InstitutesController');

	# Materials
	Route::resource('materiais', 'MaterialsController');

	# Agendas
	Route::post('agendas/download/relatorio', ['as'=>'agenda.download.excel', 'uses'=>'AgendasController@download']);
	Route::post('agendas/acao', ['as'=>'agenda.action', 'uses'=>'AgendasController@action']);
	Route::get('agendas/buscar', ['as'=>'agenda.search', 'uses'=>'AgendasController@index']);

	# Ações com pos-visita
	Route::get('pos-visitas/agenda/{agenda_id}/mostrar', ['as'=>'admin.visiters.show', 'uses'=>'AgendasController@showVisiters']);
	

	Route::resource('agendas', 'AgendasController');
	//Route::get('agendas/realizadas', ['as'=>'admin.agendas.realizadas','uses'=>'AgendasController@agendasRealizadas']);
	

	# Líderes/usuários
	Route::get('usuarios/participantes', ['as'=>'admin.usuarios.guests', 'uses'=>'UsersController@guests']);
	Route::get('usuarios/buscar', ['as'=>'users.search', 'uses'=>'UsersController@index']);

	Route::resource('usuarios', 'UsersController');

	# Usuarios administrativos
	Route::get('administrativos/usuarios', ['as'=>'admin.users','uses'=>'AdminController@indexAdmin']);
	Route::post('administrativos/usuarios', ['as'=>'admin.users.create','uses'=>'AdminController@indexAdminCreate']);
	Route::delete('administrativos/usuarios/{id}', ['as'=>'admin.users.delete','uses'=>'AdminController@deleteAdminUser']);

	# Logout
	Route::get('logout', ['as'=>'admin.logout', 'uses'=>'AdminController@logout']);

	#Pos visitas
	Route::get('pos-visitas/agenda/{agenda_id}/mostrar', ['as'=>'admin.visiters.show', 'uses'=>'AgendasController@showVisiters']);
	
	# Trasferencia de agendamento
	Route::post('agendamento/transferir/{agenda}', ['as'=>'admin.agenda.transfer', 'uses'=>'AgendasController@transfer']);
	
	# Relatórios
	Route::get('relatorios/conversoes', ['as'=>'admin.relatorios.conversion', 'uses'=>'AdminController@conversion']);
	Route::post('relatorios/conversoes', ['as'=>'admin.relatorios.conversion.post', 'uses'=>'AdminController@conversion']);
	Route::post('relatorios/pos-visitas', ['as'=>'admin.relatorios.posvisitas', 'uses'=>'AdminController@excelPosVisita']);

	Route::any('relatorios/consolidado', ['as'=>'relatorio.all', 'uses'=>'AdminController@relatorioAll']);

});

# Admin GCs
Route::group(['prefix'=>'instituto', 'before'=>'auth|auth.manager'], function(){
	// Agrupando o instituto em si
	Route::group(['prefix'=>'{slug}', 'before'=>'institute.manager'], function($slug){

		# Agendas
		Route::get('agendas', ['as'=>'instituto.agendas.index', 'uses'=>'AgendasController@index']);
		Route::post('agendas/download/relatorio', ['as'=>'instituto.agenda.download.excel', 'uses'=>'AgendasController@download']);
		Route::post('agendas/acao', ['as'=>'instituto.agenda.action', 'uses'=>'AgendasController@action']);
		Route::get('agendas/buscar', ['as'=>'instituto.agenda.search', 'uses'=>'AgendasController@index']);

		# Líderes/usuários
		Route::get('usuarios', ['as'=>'instituto.usuarios.index','uses'=>'UsersController@index']);

		# Pós visita
		Route::get('pos-visitas/pendentes', ['as'=>'visiters.pendentes', 'uses'=>'AgendasController@visiters']);
		Route::get('pos-visitas/realizados', ['as'=>'visiters.realizados', 'uses'=>'AgendasController@visiters']);
		Route::get('pos-visitas/buscar', ['as'=>'visiters.search', 'uses'=>'AgendasController@visiters']);
		Route::get('pos-visitas/agenda/{agenda_id}', ['as'=>'visiters.create', 'uses'=>'AgendasController@createVisiters']);
		
		Route::post('pos-visitas/agenda/{agenda_id}', ['as'=>'visiters.store', 'uses'=>'AgendasController@storeVisiters']);

		# Trasferencia de agendamento
		Route::post('agendamento/transferir/{agenda}', ['as'=>'agenda.transfer', 'uses'=>'AgendasController@transferIns']);

		# Logout
		Route::get('logout', ['as'=>'manager.logout', 'uses'=>'AdminController@managerLogout']);

		# Agendamento Manual
		Route::get('agendamento/manual', ['as'=>'admin.agendamento.create', 'uses'=>'AgendasController@createAgendamento']);
		
	});
	
	# Ações com pos-visita
	Route::get('pos-visitas/agenda/{agenda_id}/mostrar', ['as'=>'visiters.show', 'uses'=>'AgendasController@showVisiters']);
	Route::get('pos-visitas/agenda/{agenda_id}/editar', ['as'=>'visiters.edit', 'uses'=>'AgendasController@showVisiters']);
	Route::post('pos-visitas/agenda/{agenda_id}/update', ['as'=>'visiters.update', 'uses'=>'AgendasController@updateVisiters']);
});

Route::get('instituto/{slug}', ['as'=>'manager.index', 'uses'=>'AdminController@index']);
Route::get('instituto/{slug}/novo-gc', ['as'=>'new.manager', 'uses'=>'AdminController@indexNewManager']);
Route::post('instituto/{slug}/novo-gc', ['as'=>'register.new.manager', 'uses'=>'AdminController@registerNewManager']);


// Validators
Validator::extend('cpf', 'CustomValidator@cpf', 'CPF inválido');

