<?php

/*		if(Sentry::getUser()->hasAccess('colaborator')){
			
			$institutes = Institute::all()->groupBy('state_id');

			$view->nest('block_menu', 'layouts.partials.user.edit.menu')
				 ->nest('block_institutes', 'layouts.partials.user.edit.institutes', compact('institutes','user'));
		}*/
View::composer('layouts.partials.user.edit.institutes', function($view){
	$institutes = Institute::active()->get()->groupBy('state_id');
	$view->with('institutes', $institutes);
});

View::composer('layouts.partials.admin.navigation', function($view)
{
	$u = Sentry::getUser();
	$user = User::find($u->id);
	$slug = isset($user->manager->institute->slug) ? $user->manager->institute->slug : null ;
	$view->with('slug', $slug);
});