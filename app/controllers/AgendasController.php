<?php

class AgendasController extends BaseController {

	protected $layout = 'layouts.base';

	public function indexApp()
	{
		$user = $this->getUser();

		$block_menu = $block_institutes = null;

		$agendas_confirmadas = Agenda::whereUser_id($user->id)->onlyConfirmed()->orderBy('date_marked','ASC')->get();
		$agendas_canceladas = Agenda::whereUser_id($user->id)->getByStatus(2)->orderBy('date_marked','ASC')->get();
		$agendas_realizadas = Agenda::whereUser_id($user->id)->getByStatus(3)->orderBy('date_marked','ASC')->get();
		$agendas = Agenda::whereUser_id($user->id)->getByStatus(0)->orderBy('date_marked','ASC')->get();

		$this->layout->content = View::make('agendas.index', compact('user','agendas','agendas_confirmadas', 'agendas_realizadas', 'agendas_canceladas'))
		->nest('block_menu', 'layouts.partials.user.edit.menu');
	}


	public function index($slug=null)
	{

		$a = Agenda::query();

		// Route actions
		$search_action = 'agenda.search';
		$excel_action = 'agenda.download.excel';
		$status_action = 'agenda.action';
		$route_show_visiters = 'admin.visiters.show';

		if($slug!=null){
			// Route actions quando é instituto admin GC
			$search_action = array('instituto.agenda.search', $slug);
			$excel_action = array('instituto.agenda.download.excel',$slug);
			$status_action = array('instituto.agenda.action', $slug);
			$route_show_visiters = 'visiters.show';

			$institute = Institute::whereSlug($slug)->first();

			$a = Agenda::whereInstitute_id($institute->id);
		}

		if(Input::has('institute_id')){
			$a->whereInstitute_id(Input::get('institute_id'));
		}
		if(Input::has('status')){
			$a->whereStatus(Input::get('status'));
		}else{
			$a->whereIn('status',['0','1','2']);
		}
		if(Input::has('from') and Input::has('to')){

			$a->where('date_marked','>=', Helper::ConverterUS(Input::get('from')))
			  ->where('date_marked','<=', Helper::ConverterUS(Input::get('to')));
		}

		$agendas_all = $a->orderBy('status','ASC')->orderBy('date_marked','ASC')->get();
		$agendas = $a->orderBy('status','ASC')->orderBy('date_marked','ASC')->paginate(20);

		$institutes = Institute::orderBy('name')->lists('name','id');

		$states = State::orderBy('name')->lists('uf','id');

		$status = ['0'=>'Aguardando confirmação','1'=>'Confirmado','2'=>'Cancelado','3'=>'Realizado'];

		$route_transfer = Route::current()->getParameter('slug') != null ? 'agenda.transfer' : 'admin.agenda.transfer';

		return View::make('admin.agendas.index', compact('search_action','route_show_visiters','excel_action','status_action', 'route_transfer','slug','agendas','agendas_all','institutes', 'states','status'));
	}

	public function action()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['action'=>'required', 'agenda_ids'=>'required']);

		if($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$agendas = Agenda::whereIn('id', $data['agenda_ids'])->where('status','!=','3');
		// Justificando caso as agendas selecionadas forem canceladas
		if($data['action'] == '2'){
			
			$agendas->update(array('justify'=>$data['justify']));

			foreach ($agendas->get() as $agenda) {
				Mail::send('emails.agendas.canceled', ['agenda'=>$agenda], function($message) use($agenda){
					$message->to($agenda->user->email,$agenda->user->userdata->username)->subject('Cancelamento de agendamento Caravanas da Beleza');
				});
			}	
		}
		$agendas->update(array('status'=>$data['action']));

		// Enviando email para notificação de confirmação de agendamento ou cancelamento
		if($data['action'] == '1'){
			foreach ($agendas->get() as $agenda) {
				Mail::send('emails.agendas.confirmed', ['agenda'=>$agenda], function($message) use($agenda){
					$message->to($agenda->user->email,$agenda->user->userdata->username)->subject('Confirmação de agendamento Caravanas da Beleza');
				});
			}	
		}
		

		return Redirect::back()->withSuccess('Agendas alteradas com sucesso');
	}



	
	public function create($institute_id=null)
	{
		$user = $this->getUser();

		$block_menu =  null;

		// Pega apenas os institutos que o usuário tiver interesse
		/*$institutes = Institute::join('institute_user','institutes.id','=','institute_user.institute_id')
		->where('institute_user.user_id', $user->id)
		->orderBy('institutes.name')->lists('institutes.name','institutes.id');*/

		$institutes = Institute::active()->whereHas('users', function($q) use($user){
			$q->whereUser_id($user->id);
		})->lists('name','id');

		$display_calendar = $institute_id != null ? 'block' : 'none';

		$institute = Institute::find($institute_id);

		$view = View::make('agendas.create', compact('user','institutes', 'institute', 'institute_id','display_calendar'));
		
		if(Sentry::getUser()->hasAccess('colaborator')){

			$view->nest('block_menu', 'layouts.partials.user.edit.menu');
		}

		$this->layout->content = $view;
	}

	public function store(){
		$data = Input::all();
		

		$validator = Validator::make($data, Agenda::$rules);

		if($validator->fails()){
			if(Input::has('admin')){
				return Redirect::route('admin.agendamento.create', $data['slug'])->withErrors($validator)->withInput();
			}
			return Redirect::route('agendamento.create')->withErrors($validator)->withInput();
		}

		$agenda = Agenda::create($data);

		$user = $this->getUser();

		if(Input::has('admin')){
			$user = User::find(Input::get('user_id_admin'));
		}

		$agenda->date_marked = Helper::ConverterUS($data['date_marked']);
		$agenda->user()->associate($user);
		$agenda->save();

		// Mandando Emails para os Gcs do instituto
		$managers = Manager::whereInstitute_id($data['institute_id'])->get();

		foreach ($managers as $m) {

			Mail::send('emails.agendas.new', ['agenda'=>$agenda, 'm'=>$m], function($message) use($m){
				$message->to($m->user->email,$m->user->userdata->username)->subject('Confirmação de agendamento Caravanas da Beleza');
			});

		}

		if(Input::has('admin')){
			return Redirect::route('instituto.agendas.index', $data['slug'])->withSuccess('Agenda criada!');
		}

		return Redirect::route('agendamento.confirmed')->withSuccess('1');

	}

	public function resp()
	{
		if(!Session::has('success')){
			return Redirect::route('agendamento.create')->withError('Erro ao agendar serviço, tente novamente mais tarde.');
		}
		$this->layout->content = View::make('responses.agendamento');
	}

	public function edit()
	{
		$this->layout->content = View::make('users.edit');
	}

	public function download()
	{
		if(!Input::get('agendas')){
			return Redirect::back()->withError('Relatório vazio não pode ser gerado!');
		}
		$agendas = Agenda::find(Input::get('agendas'));
				
		Excel::create('BN_Agendamento_'.date('d_m_Y'), function($excel) use($agendas){

			$excel->sheet('Agendas', function($sheet) use($agendas) {

       			 $sheet->loadView('admin.excel.agendas',['agendas'=>$agendas]);
       		});
    	})->export('xls');

	}

	/**
	* Cadastro de pós visitas
	**/
	public function visiters($slug)
	{
		 $route = Route::current()->getPath();

		 $u = User::getAgendaInstitute($slug);

		 $institute = Institute::whereSlug($slug)->first();

		 if(strpos($route, 'realizados')){
		 	$u = User::getAgendaInstitute($slug, true);
		 }
			

		if(Input::has('cpf')){
			$u->whereHas('userdata', function($q){
				$q->whereCpf(Input::get('cpf'));
			});
		}
		$users = $u->groupBy('id')->get();


		$ags = Agenda::whereInstitute_id($institute->id);
		
		if(strpos($route, 'realizados')){
			$ags->where(function($q) use($institute){
				$q->whereStatus('3')->has('visiter')->whereInstitute_id($institute->id);
			});
		}else{
			$ags->where(function($q){
				$q->whereIn('status',['0','1'])->where('date_marked','>',date('Y-m-d'));
			})->orWhere(function($s) use($institute){
				$s->whereStatus('3')->has('visiter','=','0')->whereInstitute_id($institute->id);
			});
			
		}
		$ags->latest('date_marked');


		$agendas = $ags->paginate(15);
		
		//print_r(DB::getQueryLog());

		return View::make('admin.visiters.index',compact('users', 'route','institute','agendas'));
	}

	public function createVisiters($slug, $agenda_id)
	{
		$agenda = Agenda::find($agenda_id);
		$questions = Question::all();
		return View::make('admin.visiters.create',compact('agenda','questions'));
	}

	public function storeVisiters($slug, $agenda_id)
	{
		$data = Input::all();

		$validator = Validator::make($data, Visiter::$rules);

		if($validator->fails()){
			return Redirect::route('visiters.create', [$slug,$agenda_id])->withError('É obrigatório o preenchimento de todas as perguntas')->withInput();
		}

		$arr = [
		'q1' => $data['q1'],
		'q2' => $data['q2'],
		'q3' => $data['q3'],
		'q4' => $data['q4'],
		'q5' => $data['q5'],
		'q6' => $data['q6'],
		'q7' => $data['q7'],
		'q8' => $data['q8'],
		'q9' => $data['q9'],
		'q10' => $data['q10']
		];

		$visiter = new Visiter;
		$visiter->agenda_id = $agenda_id;
		$visiter->result = base64_encode(serialize($arr));
		$visiter->save();

		return Redirect::route('visiters.pendentes',$slug)->withSuccess('Cadastro pós visita feito com sucesso!');
	}

	public function showVisiters($id)
	{
		$visiter = Visiter::find($id);
		
		//print_r(expression)
		
		$q = unserialize(base64_decode($visiter->result));

		$questions  = Question::all();

		$view  = 'admin.visiters.edit';

		if(Route::getCurrentRoute()->getName() == 'visiters.show' or  Route::getCurrentRoute()->getName() == 'admin.visiters.show'){
			$view  = 'admin.visiters.show';
		}


		return View::make($view, compact('visiter', 'q', 'questions'));
	}

	public function updateVisiters($agenda_id)
	{

		$data = Input::all();

		$slug = Route::current()->getParameter('slug');

		$validator = Validator::make($data, Visiter::$rules);

		$visiter = Visiter::whereAgenda_id($agenda_id)->first();


		if($validator->fails()){
			return Redirect::route('visiters.edit', $visiter->id)->withError('É obrigatório o preenchimento de todas as perguntas')->withInput();
		}

		$arr = [
		'q1' => $data['q1'],
		'q2' => $data['q2'],
		'q3' => $data['q3'],
		'q4' => $data['q4'],
		'q5' => $data['q5'],
		'q6' => $data['q6'],
		'q7' => $data['q7'],
		'q8' => $data['q8'],
		'q9' => $data['q9'],
		'q10' => $data['q10']
		];

		
		$visiter->result = base64_encode(serialize($arr));
		$visiter->save();

		return Redirect::route('visiters.show', $visiter->id)->withSuccess('Cadastro pós visita editado com sucesso!');


	}

	/**
	* Funções de tranferencia de instituto 
	* Para cada administrativo GC/Admin
	**/

	public function transfer($agenda)
	{
		$institute = Institute::find(Input::get('institute_id'));

		$agenda = Agenda::find($agenda);
		
		$agenda->institute_id = Input::get('institute_id');

		$agenda->save();

		// Enviando email ao resposével GC do novo instituto
		/*Mail::send('emails.agendas.new', ['agenda'=>$agenda], function($message) use($agenda){
			$message->to()->subject('Nova agendamento tranferido');
		});*/

		
		return Redirect::route('admin.agendas.index')->withSuccess('Agendamento transferido com sucesso para o instituto '.$institute->name.' com sucesso!');

	}
	public function transferIns($slug,$agenda)
	{
				$institute = Institute::find(Input::get('institute_id'));

		$agenda = Agenda::find($agenda);
		
		$agenda->institute_id = Input::get('institute_id');

		$agenda->save();

		// Enviando email ao resposével GC do novo instituto
		/*Mail::send('emails.agendas.new', ['agenda'=>$agenda], function($message) use($agenda){
			$message->to()->subject('Nova agendamento tranferido');
		});*/

		
		return Redirect::route('instituto.agendas.index', $slug)->withSuccess('Agendamento transferido com sucesso para o instituto '.$institute->name.' com sucesso!');
		
	}

	/**
	* Agendamento manual
	*
	**/
	public function createAgendamento()
	{
		$user = null;
		if(Input::has('cpf') or Input::old('cpf') !== null ){
			$cpf = Input::old('cpf') !== null ? Input::old('cpf') : Input::get('cpf');
			$user = User::whereHas('userdata', function($q) use($cpf){
				$q->whereCpf($cpf);
			})->first();
		

		}

		$institute = Institute::whereSlug(Route::current()->getParameter('slug'))->first();

		return View::make('admin.agendas.create', compact('user', 'institute'));
	}
}