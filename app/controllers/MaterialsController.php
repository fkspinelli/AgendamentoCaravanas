<?php

class MaterialsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /institutes
	 *
	 * @return Response
	 */
 
 	protected $layout = 'layouts.base';


	public function indexApp()
	{
		$user = $this->getUser();
		$materials = Material::orderBy('id','DESC')->paginate(20);

		$this->layout->content = View::make('materials.index', compact('user', 'materials'));
	}


	public function index()
	{
		//
		$materials = Material::orderBy('id','DESC')->paginate(20);


		return View::make('admin.materials.index', compact('materials'));
	}

	public function getExtension($src)
	{
		$r = explode(".", $src)[1];
		return $r;
	}

		

	/**
	 * Show the form for creating a new resource.
	 * GET /institutes/create
	 *
	 * @return Response
	 */
	public function create()
	{

		return View::make('admin.materials.create');

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /institutes
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$attributeNames = array(
		   'name' => 'Nome',
		   'description' => 'Descrição',     
		   'src' => 'Arquivo',     
		);

		$validator = Validator::make($data, Material::$rules);

		$validator->setAttributeNames($attributeNames);

		if($validator->fails()){
			return Redirect::route('admin.materiais.create')->withErrors($validator)->withInput();
		}


		$allowedExts = array("jpeg", "jpg", "pdf", "zip");
		$arr = explode(".", $_FILES["src"]["name"]);
		$extension = end($arr);
			if ((($_FILES["src"]["type"] == "image/jpeg")
				|| ($_FILES["src"]["type"] == "image/jpg")
				|| ($_FILES["src"]["type"] == "application/zip")
				|| ($_FILES["src"]["type"] == "application/pdf"))
				&& ($_FILES['src']['error'] == '0')
				&& in_array($extension, $allowedExts)){

				$material = Material::create($data);
				
				$image = $this->upload_file($_FILES['src'], null, null, 'uploads/material');
				$material->src =  $image->file_dst_name;
				$material->save();
			} else {
				return Redirect::route('admin.materiais.create')->withErrors('Tipo de arquivo inválido. Tipos permitidos: jpg, pdf e zip.')->withInput();
			}


		return Redirect::route('admin.materiais.index')->withSuccess('Material criado com sucesso');
	}

	/**
	 * Display the specified resource.
	 * GET /institutes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /institutes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$material = Material::find($id);

		return View::make('admin.materials.edit', compact('material'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$material = Material::find($id);

		$material->name = $data['name'];
		$material->description = $data['description'];

		if ($_FILES["src"]['name'] != '') {
			$allowedExts = array("jpeg", "jpg", "pdf", "zip");
			$arr = explode(".", $_FILES["src"]["name"]);
			$extension = end($arr);
				if ((($_FILES["src"]["type"] == "image/jpeg")
				|| ($_FILES["src"]["type"] == "image/jpg")
				|| ($_FILES["src"]["type"] == "application/zip")
				|| ($_FILES["src"]["type"] == "application/pdf"))
				&& ($_FILES['src']['error'] == '0')
				&& in_array($extension, $allowedExts)){
					File::delete('uploads/material/'.$material->src);
					$image = $this->upload_file($_FILES['src'], null, null, 'uploads/material');
					$material->src =  $image->file_dst_name;
				} else {
					return Redirect::route('admin.materiais.edit',['id'=>$id])->withErrors('Tipo de arquivo inválido. Tipos permitidos: jpg, pdf e zip.')->withInput();
				}
		}

		$material->save();


		return Redirect::route('admin.materiais.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Material']));

	}
	/**
	 * Remove the specified resource from storage.
	 * DELETE /institutes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$material = Material::find($id);

		

		$material->delete();

		return Redirect::route('admin.materiais.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Material']));

	}

}