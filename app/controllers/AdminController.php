<?php

class AdminController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /admin
	 *
	 * @return Response
	 */
	//protected $layout = 'layouts.admin';

	public function index($slug=null)
	{

		return View::make('admin.index', compact('slug'));
	}

	public function indexAdmin()
	{
		$group = Sentry::findGroupByName('admin');

		$u = Sentry::findAllUsersInGroup($group);
		$users = User::whereIn('id',$u->lists('id'))->orderBy('id','DESC')->paginate(10);
		return View::make('admin.users.index', compact('users'));
	}

	public function indexAdminCreate()
	{
		$data = Input::all();

		$validator = Validator::make($data, ['username'=>'required','email'=>'required|email|unique:users','password'=>'required|confirmed']);

		if($validator->fails()){
			return Redirect::route('admin.users')->withErrors($validator)->withInput();
		}

		$u = Sentry::createUser([
			'email'=>$data['email'],
			'password'=>$data['password'],
			'activated' => true,
		]);

		$userdata = Userdata::create($data);
		$userdata->user()->associate($u);
		$userdata->save();

		$group = Sentry::findGroupByName('Admin');

		$u->addGroup($group);

		return Redirect::route('admin.users')->withSuccess('Usuário criado com sucesso!');
	}

	public function deleteAdminUser($id)
	{
		$user = User::find($id);
		$user->delete();

		return Redirect::route('admin.users')->withSuccess('Usuário apagado.');
	}

	public function indexNewManager($slug = null)
	{
		return View::make('admin.new', compact('slug'));
	}

	public function registerNewManager($slug=null){

		$data = Input::all();

		$rules = [
			'username'  => 'required|alpha_spaces',
			'email'     => 'email|unique:users',
			'password' =>'required|min:4',
		];

		$validator = Validator::make($data, $rules);

		if($validator->fails()){
			return Redirect::route('new.manager', $data['institute_slug'])->withErrors($validator)->withInput();
		}

		// Precisa antes de tud conferir se este email é de algum manager do referido instituto
		$institute = Institute::whereSlug(Input::get('institute_slug'))->first();

		$manager = Manager::whereEmail($data['email'])->whereInstitute_id($institute->id)->get();


		if($manager->count() < '1'){
			return Redirect::route('new.manager', $data['institute_slug'])->withError('Este email informado não está cadastrado na base deste instituto.')->withInput();
		}



	 	 // Let's create a user.
	    $user = Sentry::createUser(array(
	        'email'    => $data['email'],
	        'password' => $data['password'],
	        'activated' => true
	    ));

	    $u = User::find($user->id);

	    $userdata = Userdata::create($data);
	    $userdata->user()->associate($u);
	    $userdata->profession_id = '247';
	    $userdata->city_id = $institute->city_id;


	    $userdata->save();

	    Manager::whereEmail($user->email)->update(['user_id' => $user->id]);

	    $group = Sentry::findGroupByName('Manager');

	    $user->addGroup($group);

	    Sentry::login($user,false);

	    return Redirect::route('instituto.agendas.index', $data['institute_slug'])->withSuccess('Você foi cadastrado com sucesso, começe a usar seu acesso como GCdo instituto!');
	}

	public function login()
	{
		if(Input::has('institute_slug')){
			return $this->masterLogin('manager.index','instituto.agendas.index',Input::get('institute_slug'));
		}
		return $this->masterLogin('admin.index','admin.institutos.index');
	}

	public function logout(){

		return $this->masterLogout('admin.index');
	}


	public function managerLogout($slug){


		return $this->masterLogout('manager.index', $slug);

	}

	public function conversion()
	{
		$i = Institute::orderBy('name','ASC');
		$i_t = Institute::orderBy('name','ASC');

		if(Input::has('state_id')){
			$i->whereState_id(Input::get('state_id'));
			$i_t->whereState_id(Input::get('state_id'));
		}
		if(Input::has('from') and Input::has('to')){
			$i->getByVisitorRangeDate(Helper::ConverterUS(Input::get('from')), Helper::ConverterUS(Input::get('to')));
			$i_t->getByVisitorRangeDate(Helper::ConverterUS(Input::get('from')), Helper::ConverterUS(Input::get('to')));

		}
		if(Input::has('institute_id')){
			$i->whereId(Input::get('institute_id'));
			$i_t->whereId(Input::get('institute_id'));
		}

		$institutes = $i->groupBy('id')->paginate(25);
		$institutes_total = $i_t->groupBy('id')->get();

		$ins = Institute::orderBy('name')->lists('name','id');

		$states = State::orderBy('name')->lists('name','id');

		if(Request::method() == 'POST'){
			Excel::create('Relatorio_Institutos_'.date('d_m_Y'), function($excel) use($institutes_total,$states,$ins){

				$excel->sheet('Relatorio', function($sheet) use($institutes_total,$states,$ins) {

	       			 $sheet->loadView('admin.excel.relatorios',['institutes'=>$institutes_total, 'states'=>$states, 'ins'=>$ins]);
	       		});

	    	})->export('xls');

	    	return Redirect::route('admin.relatorios.conversion');
		}

		return View::make('admin.relatorios.index', compact('institutes','states','ins'));
		
	}

	public function excelPosVisita()
	{
		if(!Input::has('institute_id')){
			return Redirect::route('admin.relatorios.conversion')->withError('Selecione ao menos  um instituto válido');
		}

		$institute = Institute::find(Input::get('institute_id'));

		$questions = Question::all();

		Excel::create('BN_Agendamento_pos_visitas_'.Slugify::slugify($institute->name).'_'.date('d_m_Y'), function($excel) use($institute, $questions){

			$excel->sheet(str_limit('Pos_Visitas_'.Slugify::slugify($institute->name),27), function($sheet) use($institute, $questions) {

       			 $sheet->loadView('admin.excel.posVisitas',['institute'=>$institute, 'questions'=>$questions]);
       		});

    	})->export('xls');
	}

	public function relatorioAll()
	{
		if(!Input::has('period')){
			return Redirect::back()->withError('Selecione período');
		}

		$visiters = Visiter::orderBy('id','desc')
		->where('created_at', '>=', Helper::SubtrairMeses(date('Y-m-d'), Input::get('period')))
		->where('created_at', '<=', date('Y-m-d'))
		->get();

		if($visiters->count() < 1){
			return Redirect::back()->withError('Não encontrado para este período');
		}

		$questions = Question::all();

		Excel::create('BN_Agendamento_pos_visitas_'.date('d_m_Y'), function($excel) use($visiters, $questions){

			$excel->sheet('PosVisitas', function($sheet) use($visiters, $questions) {

       			 $sheet->loadView('admin.excel.posVisitasAll',['visiters'=>$visiters, 'questions'=>$questions]);
       		});

    	})->export('xls');

    	return Redirect::back();
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /admin/create
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /admin
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /admin/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /admin/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
