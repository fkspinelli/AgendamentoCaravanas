<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function masterLogin($input_route, $output_route, $slug=null){
		
		$credentials = ['email'=>Input::get('email'), 'password'=>Input::get('password')];

		$validator = Validator::make($credentials, ['email'=>'required|email','password'=>'required']);

		if($validator->fails()){
			return Redirect::route($input_route, $slug)->withInput()->withErrors($validator);
		}

		try{
			//logando o usuario e redirecionando
			Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));
			//echo $output_route;
			return Redirect::route($output_route, $slug);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::route($input_route, $slug)->with('error','Usuário não encontrado')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::route($input_route, $slug)->with('error','Usuário não ativado!')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    return Redirect::route($input_route, $slug)->with('error','Usuário foi temporariamente suspenso, por muitas tentativas de login não sucedidas. Entre novamente mais tarde')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    return Redirect::route($input_route, $slug)->with('error','Usuário foi banido, entre em contato com dev@dizain.com.br.')->withInput(Input::except('password'));
		}
	}

	public function masterLogout($route, $param=null){
		Sentry::logout();

		if($param!=null){
			return Redirect::route($route, $param);
		}
		
		return Redirect::route($route);

	}

	public function upload_file($src, $w=120, $h=120, $folder='uploads', $filename=null){

		$image = new Upload($src);

		if($filename != null){
			$image->file_new_name_body = $filename;
		}else{
			$image->file_new_name_body = uniqid(rand());
		}
		
		$image->file_safe_name = true;
		if ($h != null && $w != null) {
			$image->image_resize          = true;
			$image->image_ratio_crop      = true;
			$image->image_y               = $h;
			$image->image_x               = $w;
		}
		$image->process($folder);
		if($image->processed){
			return $image;
		}

		return $image->error;


	}

	public function getUser(){
		$u = Sentry::getUser();
		$user = User::find($u->id);
		return $user;
	}

}
