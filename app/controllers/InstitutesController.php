<?php

class InstitutesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /institutes
	 *
	 * @return Response
	 */
 
 	protected $layout = 'layouts.base';

	public function index()
	{
		//
		$institutes = Institute::orderBy('id','DESC')->paginate(20);


		return View::make('admin.institutes.index', compact('institutes'));
	}

		

	/**
	 * Show the form for creating a new resource.
	 * GET /institutes/create
	 *
	 * @return Response
	 */
	public function create()
	{
		$states = State::orderBy('id','DESC')->lists('uf','id');

		return View::make('admin.institutes.create', compact('states'));

	}

	/**
	 * Store a newly created resource in storage.
	 * POST /institutes
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Institute::$rules);

		if($validator->fails()){
			return Redirect::route('admin.institutos.create')->withErrors($validator)->withInput();
		}

		$institute = Institute::create($data);

		//Slug
		$i = Institute::whereSlug(Slugify::slugify($data['name']))->get();

		$institute->slug = isset($i->id) ?  uniqid().'-'.Slugify::slugify($data['name']) : Slugify::slugify($data['name']);
		$institute->active = '1';

		$institute->save();

		// Criando managers
		$resp = explode(',', trim($data['responsaveis']));

		foreach ($resp as $r) {
			$u = User::whereEmail($r)->first();
			$manager = new Manager;
			$manager->email = $r;
			$manager->user_id = isset($u->id) ? $u->id : 0 ;
			$manager->institute()->associate($institute);
			$manager->save();

			// Manda email para o novo GC
		}

		return Redirect::route('admin.institutos.index')->withSuccess('Instituto criado com sucesso');
	}

	/**
	 * Display the specified resource.
	 * GET /institutes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /institutes/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$states = State::orderBy('id','DESC')->lists('uf','id');		

		$institute = Institute::find($id);

		$cities = City::whereState_id($institute->state_id)->lists('name','id');

		$m = Manager::whereInstitute_id($institute->id)->lists('email');

		$resp = implode(',', $m);

		return View::make('admin.institutes.edit', compact('institute','states','cities','resp'));
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /categories/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$data = Input::all();

		$institute = Institute::find($id);

		if($institute->name != Input::get('name')){
			$institute->slug = Slugify::slugify(Input::get('name'));
			$institute->save();
		}

		$institute->update($data);

		$i = Institute::whereSlug(Slugify::slugify($data['name']))->get();

		$institute->slug = isset($i->id) ?  uniqid().'-'.Slugify::slugify($data['name']) : Slugify::slugify($data['name']);

		$institute->active = isset($data['active']) ? '1' : '0';

		$institute->save();

		// Editando managers
		$m = Manager::whereInstitute_id($institute->id)->lists('email');

		$existing = implode(',', $m);

		if(trim($data['responsaveis']) != trim($existing)){

			$institute->managers()->delete();

			$resp = explode(',', trim($data['responsaveis']));

			foreach ($resp as $r) {
				if(!in_array(trim($r), $m)){
					//manda Email para o novo GC
				}
				$u = User::whereEmail(trim($r))->first();
				$manager = new Manager;
				$manager->email = trim($r);
				$manager->user_id = isset($u->id) ? $u->id : 0 ;
				$manager->institute()->associate($institute);
				$manager->save();
			}
		}
		return Redirect::route('admin.institutos.edit',['id'=>$id])->withSuccess(Lang::get('crud.update.success', ['element'=>'Instituto']));

	}
	/**
	 * Remove the specified resource from storage.
	 * DELETE /institutes/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$area = Institute::find($id);

		

		$area->delete();

		return Redirect::route('admin.institutos.index')->withSuccess(Lang::get('crud.destroy.success', ['element'=>'Instituto']));

	}

}