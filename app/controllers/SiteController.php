<?php

class SiteController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	protected $layout = 'layouts.base';

	public function index()
	{
		$this->layout->content = View::make('users.login');
	}

	
	public function contact()
	{
		$this->layout->content = View::make('site.contact');
	}

	public function terms()
	{
		$this->layout->content = View::make('site.terms');
	}
}
