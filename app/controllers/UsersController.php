<?php

class UsersController extends BaseController {

	protected $layout = 'layouts.base';

	public function indexApp()
	{
		//Listagem de usuários
		$user = $this->getUser();

		$leaders = User::has('institutes')->getLeadersNearest($user->userdata->city->state->id)->whereNotIn('users.id',[$user->id])->paginate(15);

		$this->layout->content = View::make('users.index', compact('user','leaders'));
	}

	public function index($slug=null)
	{
		$alias = 'Líderes';

		$u = User::has('institutes')->orderBy('id','DESC');

		if($slug!=null){
			$institute = Institute::whereSlug($slug)->first();

			// Pega apenas os líders do insituto em questão
			$u = User::whereHas('institutes', function($q) use($institute){
				$q->whereIn('institutes.id',array($institute->id));
			})->orderBy('id','DESC');

		}
		if(Input::has('state_id') and !Input::get('city_id')){
			$u->whereHas('userdata', function($q){
				$q->join('cities','userdatas.city_id','=','cities.id')
				->join('states','cities.state_id','=','states.id')
				->where('states.id',Input::get('state_id'));
			});
		}
		$cities = array();
		if(Input::has('city_id')){
			$u->whereHas('userdata', function($q){
				$q->whereCity_id(Input::get('city_id'));
			});
			$cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name','id');
		}

		if(Input::has('activity')){
			$u->where('lead_inactive', Input::get('activity'));
		}
		$states = State::orderBy('name')->lists('uf','id');
		$users = $u->paginate(25);
		$users_all = $u->get();

		return View::make('admin.users.leaders', compact('users', 'users_all', 'alias','states','cities'));

	}

	public function guests($slug=null)
	{
		$alias = 'Participantes';

		$group = Sentry::findGroupByName('guest');

		$us = Sentry::findAllUsersInGroup($group)->lists('id');

		$u = User::whereIn('id',$us);
		$cities = array();
		if(Input::has('state_id') and !Input::get('city_id')){
			$u->whereHas('userdata', function($q){
				$q->join('cities','userdatas.city_id','=','cities.id')
				->join('states','cities.state_id','=','states.id')
				->where('states.id',Input::get('state_id'));
			});
			$cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name','id');
		}

		if(Input::has('city_id')){
			$u->whereHas('userdata', function($q){
				$q->whereCity_id(Input::get('city_id'));
			});
			$cities = City::whereState_id(Input::get('state_id'))->orderBy('name')->lists('name','id');
		}
		$states = State::orderBy('name')->lists('uf','id');


		$users = $u->paginate(25);
		$users_all = $u->get();


		return View::make('admin.users.leaders', compact('users', 'users_all','alias','states','cities'));
	}

	public function login()
	{
		$this->layout->content = View::make('users.login');
	}

	public function postLogin()
	{

		$credentials = ['cpf'=>Input::get('cpf'), 'birthday'=>Input::get('birthday')];

		$validator = Validator::make($credentials, ['cpf'=>'required','birthday'=>'required|date_format:"d/m/Y"']);

		if($validator->fails()){
			return Redirect::route('user.login')->withInput()->withErrors($validator);
		}

		$u = Userdata::whereBirthday(Helper::ConverterUS($credentials['birthday']))->whereCpf($credentials['cpf'])->first();

		if(!isset($u->id)){
			return Redirect::route('user.login')->withInput()->withError('Usuário não encontrado');
		}

		try{
			//logando o usuario e redirecionando
			$user = Sentry::findUserById($u->user_id);

			Sentry::login($user);


			if($user->hasAccess('colaborator')):

				return Redirect::route('agendamento.create');

			endif;

			return Redirect::route('institutes.leaders');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::route('user.login')->with('error','Usuário não encontrado')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::route('user.login')->with('error','Usuário não ativado!')->withInput(Input::except('password'));
		}
	}

	public function create()
	{
		$professions = Profession::orderby('name', 'ASC')->lists('name','id');

		$states = State::orderby('name', 'ASC')->lists('uf','id');

		$cities = array();


		if(Input::old('state_id')){
			$cities = City::whereState_id(Input::old('state_id'))->lists('name','id');

		}

		$view = View::make('users.create', compact('professions','states', 'cities'));

		if(Route::getCurrentRoute()->getName() == 'users.create'){

			$institutes = Institute::active()->get()->groupBy('state_id');

			$view->nest('introduction', 'layouts.partials.user.create.leader')
			->nest('loop_institutes', 'layouts.partials.user.common.institutes', compact('institutes'));

		}else{
			$view->nest('introduction', 'layouts.partials.user.create.guest');
		}

		$this->layout->content = $view;
	}

	public function store()
	{
		$data = Input::all();

		$validator = Validator::make($data, Userdata::$rules);

		$docvalid = new ValidaDoc(Input::get('cpf'));

		// habilitando rotas e retornos
		$route = 'guests.create';
		$route_output = 'institutes.leaders';
		$confirm_response = 'user.register.success.guest';
		$email_response = 'emails.users.register-guest';

		// Pegando o usuário líder e habilitando rotas e retornos
		if($data['usergroup'] == 'users.create'){
			$validator_inst = Validator::make($data, ['institutes'=>'required']);

			$route = 'users.create';
			$route_output = 'users.register.return';
			$confirm_response = 'user.register.success.colaborator';
			$email_response = 'emails.users.register-leader';

			if($validator_inst->fails()){

				return Redirect::route($route)->withErrors($validator_inst)->withInput();
			}
		}

		if($validator->fails()){
			return Redirect::route($route)->withErrors($validator)->withInput();
		}

		if(!$docvalid->valida()){
			return Redirect::route($route)->withError('CPF inválido')->withInput();
		}


		try
		{
		    // Let's register a user.
		    $user = Sentry::register(array(
		        'email'    => Input::has('email') ? $data['email'] : uniqid().'@bn.com.br',
		        'password' => 'bnagendamento2016'
		    ),true);

		    $u = User::find($user->id);

		    $userdata = Userdata::create($data);
		    $userdata->birthday = Helper::ConverterUS($data['birthday'], true);

		    // Subindo imagem
		    if(Input::hasFile('src')):
				$image = $this->upload_file($_FILES['src']);
				$image =  $image->file_dst_name;
			    $userdata->src = $image;
		    endif;

		    $userdata->user()->associate($u);
		    $userdata->save();

		    // Assinalando grupos
		    if($data['usergroup'] == 'users.create'){
		    	$group = Sentry::findGroupByName('Colaborator');

			    //Salvando institutos
			   	$u->institutes()->attach($data['institutes']);
			   	$u->save();
		   	}else{
		   		$group = Sentry::findGroupByName('Guest');
		   	}

		   	// Assign the group to the user
			$user->addGroup($group);


		    // Let's get the activation code
		   // $activationCode = $user->getActivationCode();

			Sentry::login($user);

		    // Send activation code to the user so he can activate the account
		    Mail::send($email_response, array('user'=>$u), function($message){

				$message->to(Input::get('email'),Input::get('name'))->subject('Confirmação de cadastro');

			});

			return Redirect::route($route_output)->withSuccess(Lang::get($confirm_response));
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		     return Redirect::route($route)->withSuccess(Lang::get('user.register.required.login'))->withInput();
		}
		catch (Cartalyst\Sentry\Users\PasswordrequireduiredException $e)
		{
		     return Redirect::route($route)->withSuccess(Lang::get('user.register.required.pass'))->withInput();
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		     return Redirect::route($route)->withSuccess(Lang::get('user.register.exists'))->withInput();
		}


	}

	public function uploadImage()
	{
		$file = Input::file('file');
	}

	public function registerReturn()
	{

		$this->layout->content = View::make('responses.register');
	}

	public function activation($activationCode)
	{

		 try
		{
		     $user = Sentry::findUserByActivationCode($activationCode);

		    if ($user->attemptActivation($activationCode))
		    {
		        // User activation passed
		        return Redirect::route('user.login')->withSuccess(Lang::get('user.activated'));

		    }
		    else
		    {
		        // User activation failed
		         return Redirect::route('user.register')->withError(Lang::get('user.notactivated'));
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		   return Redirect::route('user.register')->withError(Lang::get('user.notfound'));
		}
	}

	public function frontEdit()
	{

		$user = $this->getUser();

		$professions = Profession::orderby('name', 'ASC')->lists('name','id');

		$states = State::orderby('name', 'ASC')->lists('uf','id');

		$cities = City::whereState_id($user->userdata->city->state->id)->orderby('name', 'ASC')->lists('name','id');


		$block_menu = $block_institutes = null;

		$text_self = Sentry::getUser()->hasAccess('colaborator') ? 'Sou líder' : 'Sou participante';

		$view = View::make('users.edit', compact('block_menu', 'block_institutes', 'text_self', 'user','professions','states','cities'));


		if(Sentry::getUser()->hasAccess('colaborator')){


			$view->nest('block_menu', 'layouts.partials.user.edit.menu')
				 ->nest('block_institutes', 'layouts.partials.user.edit.institutes', compact('institutes','user'));
		}

		$this->layout->content = $view;

	}

	public function frontUpdate($id)
	{
		$data = Input::all();

		$user = User::find($id);

		$validator = Validator::make($data, Userdata::$edit_rules);

		$spec = ['email'=>'required|email|unique:users,email,'.$id.'',
		'cpf'  => 'required|unique:userdatas,cpf,'.$user->userdata->id.''];


		$validator_email = Validator::make($data, $spec);

		$docvalid = new ValidaDoc(Input::get('cpf'));

		if($validator->fails()){
			return Redirect::route('user.front.edit')->withErrors($validator)->withInput();
		}

		if($validator_email->fails()){
			return Redirect::route('user.front.edit')->withErrors($validator_email)->withInput();
		}

		if(!$docvalid->valida()){
			return Redirect::route('user.front.edit')->withError('CPF inválido')->withInput();
		}

		if(empty($data['institutes']))
		{
			return Redirect::route('user.front.edit')->withError('Selecione institutos')->withInput();
		}


		// Editando
		$user->email = $data['email'];

		if(Sentry::getUser()->hasAccess('colaborator')):

			$institutes = Institute::all()->lists('id');

			$user->institutes()->detach($institutes);
			$user->institutes()->attach($data['institutes']);
		endif;

		$user->save();

		$userdata = Userdata::whereUser_id($user->id)->first();

		$userdata->update($data);
		$userdata->birthday = Helper::ConverterUS($data['birthday'], true);

		if(Input::file('src', null) != null){

			File::delete('uploads/'.$userdata->src);

			$image = $this->upload_file($_FILES['src']);
			$image =  $image->file_dst_name;
		    $userdata->src = $image;

		}
		$userdata->save();

		return Redirect::route('user.front.edit')->withSuccess(Lang::get('user.edit.success'));
	}

	public function updatePass($id)
	{
		$data =Input::all();

		$validate = Validator::make($data, ['password'=>'required|min:4','password_confirmation'=>'required|same:password'], ['password.required'=>'Favor preencher senha','password.min'=>'Senha deve conter mais de 4 caracteres','password_confirmation.required'=>'Favor preencher confirmação de senha','password_confirmation.same'=>'Sua confirmação deve ser igual ao campo senha']);

		if($validate->fails()){
			return Redirect::back()->withInput()->withErrors($validate);
		}

		$user = User::find($id);

		$user->password = Hash::make($data['password']);
		$user->save();

		return Redirect::route('user.front.edit')->withSuccess('Senha alterada com sucesso.');
	}

	public function updateGroup(){
		$user = Sentry::getUser();

		$colaborators = Sentry::findGroupByName('Colaborator');
		$guests = Sentry::findGroupByName('Guest');

		if($user->inGroup($colaborators)){
			$user->removeGroup($colaborators);
			$user->addGroup($guests);
			$u  = $this->getUser();
			$institutes = Institute::all()->lists('id');
			$u->institutes()->detach($institutes);
			$u->save();
			$response = 'user.edit.group.leader';
		}else{
			$user->removeGroup($guests);
			$user->addGroup($colaborators);
			$response = 'user.edit.group.guest';
		}

		return Redirect::route('user.front.edit')->withSuccess(Lang::get($response));

	}

	public function download(){

		if(!Input::get('users')){
			return Redirect::back()->withError('Relatório vazio não pode ser gerado!');
		}

		$alias = Input::get('alias');

		if($alias == 'Líderes'){

			$users = User::has('institutes')->orderBy('id','DESC')->get();

		}else{

			$group = Sentry::findGroupByName('guest');

			$us = Sentry::findAllUsersInGroup($group)->lists('id');

			$users = User::whereIn('id',$us)->orderBy('id','DESC')->get();
		}

		Excel::create('BN_Agendamento_Usuarios_'.date('d_m_Y'), function($excel) use($users,$alias){

			$excel->sheet('Usuários', function($sheet) use($users,$alias) {

       			 $sheet->loadView('admin.excel.users',['users'=>$users, 'alias'=>$alias]);
       		});
    	})->export('xls');
	}

	public function logout()
	{

		return $this->masterLogout('user.login');
	}


	public function destroy($id)
	{
		$user = User::find($id);

		$user->delete();

		return Redirect::route('admin.usuarios.index')->withSuccess('Apagado com sucesso');
	}

}
